set terminal pngcairo  background rgb 'white' enhanced font "arial,10" fontscale 1.0 size 400, 400 
set output 'nf.png'

# title
set title "Change Detection" font "arial, 15"

# y label
set ylabel "Accuracy (%)" font "arial, 15"
set yrange [ 0.0000 : 1.0000 ] noreverse nowriteback
set ytics 0.1 nomirror

# x label
set xrange [ 0.00000 : 7.0000 ] noreverse nowriteback
set xtics nomirror

set bar 1.000000 front

set boxwidth 0.2 absolute


#unset xtics

# set style circle radius graph 0.02, first 0.00000, 0.00000 
# set style ellipse size graph 0.05, 0.03, first 0.00000 angle 0 units xy
# set style textbox transparent margins  1.0,  1.0 border

# unset logscale
# unset paxis 1 tics
# unset paxis 2 tics
# unset paxis 3 tics
# unset paxis 4 tics
# unset paxis 5 tics
# unset paxis 6 tics
# unset paxis 7 tics

# set paxis 1 range [ * : * ] noreverse nowriteback
# set paxis 2 range [ * : * ] noreverse nowriteback
# set paxis 3 range [ * : * ] noreverse nowriteback
# set paxis 4 range [ * : * ] noreverse nowriteback
# set paxis 5 range [ * : * ] noreverse nowriteback
# set paxis 6 range [ * : * ] noreverse nowriteback
# set paxis 7 range [ * : * ] noreverse nowriteback

# set colorbox vertical origin screen 0.9, 0.2, 0 size screen 0.05, 0.6, 0 front  noinvert bdefault

x = 0.0

## Last datafile plotted: "candlesticks.dat"
plot 'nf.dat' using 1:3:2:6:5:xticlabels(7) with candlesticks lt 1 lw 2 title 'netflow' whiskerbars, \
      ''        using 1:4:4:4:4 with candlesticks lt -1 lw 2 notitle