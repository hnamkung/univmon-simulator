#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>

#include "app/netro_test.h"
#include "app/basic_flooding.h"
#include "app/tcp_syn.h"
#include "app/udp_change.h"
#include "app/dns_amplification.h"
#include "app/entropy.h"

using namespace std;

int main(int argc, char* argv[])
{
	srand (10);
	srand (time(NULL));
	if(argc < 3) {
		printf("usage : ./simulator [app_name] [pcap_folder]\n");
	}
	else {
		if(strcmp(argv[1], "netro_test") == 0) {
			netro_test_main(argc, argv);
		}
		else if(strcmp(argv[1], "basic_flooding") == 0) {
			basic_flooding_main(argc, argv);
		}
		else if(strcmp(argv[1], "tcp_syn") == 0) {
			tcp_syn_main(argc, argv);
		}
		else if(strcmp(argv[1], "udp_change") == 0) {
			udp_change_main(argc, argv);
		}
		else if(strcmp(argv[1], "dns_amplification") == 0) {
			dns_amplification_main(argc, argv);
		}
		else if(strcmp(argv[1], "entropy") == 0) {
			entropy_main(argc, argv);
		}
	}
	return 0;
}
