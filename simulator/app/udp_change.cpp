#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <list>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>

#define CHANGE_DETECTION_RATE 0.01
#define NETFLOW_SAMPLING_RATE 1000

#include "udp_change.h"

#include "../library/library.h"
#include "../library/countSketch.h"
#include "../library/heavyHitters.h"
#include "../library/detectionHelper.h"
#include "../library/resultHelper.h"

using namespace std;

void udp_change_ground_truth();
void udp_change_netflow();
void udp_change_univmon();

detectionHelper udp_change_detectionHelper = detectionHelper();
resultHelper udp_change_resultHelper = resultHelper();


void udp_change_main(int argc, char* argv[])
{
    if(argc < 3) {
        printf("usage : ./simulator udp_change [pcap folder name]\n");
        return;
    }

    char *pcap_folder_name = argv[2];
    char pcap_file_name[100];

    int pcap_count = 0;

    DIR *d;
    struct dirent *dir;
    d = opendir(pcap_folder_name);
    while ((dir = readdir(d)) != NULL)
    {
        char* file_name = dir->d_name;
        if(strstr(file_name, ".pcap") != NULL) {
            sprintf(pcap_file_name, "%s/%s", pcap_folder_name, file_name);
            if(pcap_count == 0) {
                char temp[100];
                sprintf(temp, "%s", pcap_file_name);
                udp_change_detectionHelper.pcap_file_name_0 = temp;
                pcap_count++;
            }
            else if(pcap_count == 1) {
                udp_change_detectionHelper.pcap_file_name_1 = pcap_file_name;

                udp_change_detectionHelper.reset();
                udp_change_resultHelper.reset();

                cout << "pcap 0 : " << udp_change_detectionHelper.pcap_file_name_0 << endl;
                cout << "pcap 1 : " << udp_change_detectionHelper.pcap_file_name_1 << endl;
                pcap_count = 0;
                udp_change_ground_truth();
                udp_change_netflow();
                udp_change_univmon();

                // udp_change_resultHelper.print_result();

                // temporary
                break;
            }
        }
    }
    closedir(d);
}

void udp_change_ground_truth()
{
    vector<Packet_Elem> &packets0_gt = udp_change_detectionHelper.packets0_gt;
    map <uint32_t, int> &packetMap0_gt = udp_change_detectionHelper.packetMap0_gt;

    vector<Packet_Elem> &packets1_gt = udp_change_detectionHelper.packets1_gt;
    map <uint32_t, int> &packetMap1_gt = udp_change_detectionHelper.packetMap1_gt;

    ParseConfig pconfig = ParseConfig();
    pcapParse(udp_change_detectionHelper.get_file_name_0(), packets0_gt, 1, packetMap0_gt, OPTION_SIZE, pconfig);
    pcapParse(udp_change_detectionHelper.get_file_name_1(), packets1_gt, 1, packetMap1_gt, OPTION_SIZE, pconfig);

    int totalChange = 0;

    map <uint32_t, int>::iterator it;

    for (it = packetMap1_gt.begin(); it != packetMap1_gt.end(); ++it) {
        map <uint32_t, int>::iterator it_change = packetMap0_gt.find(it->first);
        if (it_change!=packetMap0_gt.end()) {
            totalChange += abs(it -> second - it_change -> second);
        } else {
            totalChange += it -> second;
        }
    }

    list<topk_elem> sorted_list;

    int heavyChange = (int)((double)totalChange * CHANGE_DETECTION_RATE);
    printf("Total change is %d \n",totalChange);
    printf("Heavy change is %d \n",heavyChange);

    int change = 0;

    for (it = packetMap1_gt.begin(); it != packetMap1_gt.end(); ++it) {
        uint32_t feature = it->first;
        map <uint32_t, int>::iterator it_change = packetMap0_gt.find(feature);
        if (it_change!=packetMap0_gt.end()) {
            change = abs(it -> second - it_change -> second);
        } else {
            change = it -> second;
        }
        if(change > heavyChange) {
            udp_change_resultHelper.gt_result[feature] = change;
        }
    }
    udp_change_resultHelper.gt_print();
}


void udp_change_netflow()
{
    vector<Packet_Elem> &packets0_nf = udp_change_detectionHelper.packets0_nf;
    map <uint32_t, int> &packetMap0_nf = udp_change_detectionHelper.packetMap0_nf;

    vector<Packet_Elem> &packets1_nf = udp_change_detectionHelper.packets1_nf;
    map <uint32_t, int> &packetMap1_nf = udp_change_detectionHelper.packetMap1_nf;

    ParseConfig pconfig = ParseConfig();
    pcapParse(udp_change_detectionHelper.get_file_name_0(), packets0_nf, NETFLOW_SAMPLING_RATE, packetMap0_nf, OPTION_SIZE, pconfig);
    pcapParse(udp_change_detectionHelper.get_file_name_1(), packets1_nf, NETFLOW_SAMPLING_RATE, packetMap1_nf, OPTION_SIZE, pconfig);

    int totalChange = 0;

    map <uint32_t, int>::iterator it;

    for (it = packetMap1_nf.begin(); it != packetMap1_nf.end(); ++it) {
        map <uint32_t, int>::iterator it_change = packetMap0_nf.find(it->first);
        if (it_change!=packetMap0_nf.end()) {
            totalChange += abs(it -> second - it_change -> second);
        } else {
            totalChange += it -> second;
        }
    }

    list<topk_elem> sorted_list;

    int heavyChange = (int)((double)totalChange * CHANGE_DETECTION_RATE);
    printf("Total change is %d \n",totalChange);
    printf("Heavy change is %d \n",heavyChange);

    int change = 0;

    for (it = packetMap1_nf.begin(); it != packetMap1_nf.end(); ++it) {
        uint32_t feature = it->first;
        map <uint32_t, int>::iterator it_change = packetMap0_nf.find(feature);
        if (it_change!=packetMap0_nf.end()) {
            change = abs(it -> second - it_change -> second);
        } else {
            change = it -> second;
        }
        if(change > heavyChange) {
            udp_change_resultHelper.nf_result[feature] = change;
        }
    }
    udp_change_resultHelper.nf_print();
}

void udp_change_univmon()
{
    vector<Packet_Elem> &packets0_gt = udp_change_detectionHelper.packets0_gt;
    map <uint32_t, int> &packetMap0_gt = udp_change_detectionHelper.packetMap0_gt;

    vector<Packet_Elem> &packets1_gt = udp_change_detectionHelper.packets1_gt;
    map <uint32_t, int> &packetMap1_gt = udp_change_detectionHelper.packetMap1_gt;

    vector<countSketch> level_countSketch0, level_countSketch1;
    vector<heavyHitters> level_heavyHitters0, level_heavyHitters1;

    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_countSketch0.push_back(countSketch());
        level_heavyHitters0.push_back(heavyHitters());

        level_countSketch1.push_back(countSketch());
        level_heavyHitters1.push_back(heavyHitters());

        level_countSketch0[i].sampling_hash = udp_change_detectionHelper.level_countSketch[i].sampling_hash;
        level_countSketch0[i].index_hash = udp_change_detectionHelper.level_countSketch[i].index_hash;
        level_countSketch0[i].res_hash = udp_change_detectionHelper.level_countSketch[i].res_hash;

        level_countSketch1[i].sampling_hash = udp_change_detectionHelper.level_countSketch[i].sampling_hash;
        level_countSketch1[i].index_hash = udp_change_detectionHelper.level_countSketch[i].index_hash;
        level_countSketch1[i].res_hash = udp_change_detectionHelper.level_countSketch[i].res_hash;
    }

    get_countsketch_and_heavyhitter(packets0_gt, level_countSketch0, level_heavyHitters0, OPTION_SIZE);

    get_countsketch_and_heavyhitter_change(level_countSketch0, packets1_gt, level_countSketch1, level_heavyHitters1, OPTION_SIZE);

    // printf("change detection print1\n");
    // level_heavyHitters1[0].topk_print();
    int gsum = get_gsum(level_countSketch1, level_heavyHitters1);

    int totalChange = gsum;
    int change = 0;

    int heavyChange = (int)((double)totalChange * CHANGE_DETECTION_RATE);
    printf("Total change is %d \n",totalChange);
    printf("Heavy change is %d \n",heavyChange);

    vector<topk_elem>::iterator it;
    for (it = level_heavyHitters1[0].topk.begin(); it != level_heavyHitters1[0].topk.end(); it++) {
        uint32_t feature = it->feature;
        uint32_t estimate = it->estimate;
        if(estimate > heavyChange) {
            udp_change_resultHelper.um_result[feature] = estimate;
        }
    }
    udp_change_resultHelper.um_print();
}
