#ifndef __NETRO_H

#define __NETRO_H

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <list>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include <sys/stat.h>

#include "../library/library.h"
#include "../library/countSketch.h"
#include "../library/heavyHitters.h"
#include "../library/detectionHelper.h"
#include "../library/resultHelper.h"
#include "../library/netro_test_data_printout.h"

void netro_test_main(int argc, char* argv[]);

#endif