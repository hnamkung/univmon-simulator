#include "netro_test.h"

#define THRESHOLD (1 * 1024 * 1024) // 5MB
#define NETFLOW_SAMPLING_RATE 1000

using namespace std;

void nt_ground_truth();
void nt_netflow();
void nt_univmon();

// gt : ground truth
// nf : netflow
// um : univ mon

detectionHelper nt_detectionHelper;
resultHelper nt_resultHelper;

void netro_test_main(int argc, char* argv[])
{
	if(argc < 3) {
		printf("usage : ./simulator netro_test [pcap folder name]\n");
		return;
	}
    
	char *pcap_folder_name = argv[2];
    char pcap_file_name[100];

    DIR *d;
    struct dirent *dir;
    d = opendir(pcap_folder_name);
    while ((dir = readdir(d)) != NULL)
    {
        char* file_name = dir->d_name;
        if(strstr(file_name, ".pcap") != NULL) {
            sprintf(pcap_file_name, "%s/%s", pcap_folder_name, file_name);

            nt_detectionHelper.reset();
            nt_detectionHelper.pcap_file_name = pcap_file_name;

            nt_resultHelper.reset();

            nt_ground_truth();
            nt_netflow();
            nt_univmon();

            // nt_resultHelper.print_result();
            // temporary
            break;
        }
    }
    closedir(d);
}

void nt_ground_truth()
{
	vector<Packet_Elem> &packets_gt = nt_detectionHelper.packets_gt;
	map <uint32_t, int> &packetMap_gt = nt_detectionHelper.packetMap_gt;

    ParseConfig pconfig = ParseConfig();
	pcapParse(nt_detectionHelper.get_file_name(), packets_gt, 1, packetMap_gt, OPTION_SIZE, pconfig);

	map <uint32_t, int>::iterator it;
    for (it = packetMap_gt.begin(); it != packetMap_gt.end(); ++it) {
        if ( it->second > THRESHOLD) {
            nt_resultHelper.gt_result[it->first] = it->second;
        }
    }
}

void nt_netflow()
{
	vector<Packet_Elem> &packets_nf = nt_detectionHelper.packets_nf;
	map <uint32_t, int> &packetMap_nf = nt_detectionHelper.packetMap_nf;

    ParseConfig pconfig = ParseConfig();
	pcapParse(nt_detectionHelper.get_file_name(), packets_nf, NETFLOW_SAMPLING_RATE, packetMap_nf, OPTION_SIZE, pconfig);

	map <uint32_t, int>::iterator it;
    for (it = packetMap_nf.begin(); it != packetMap_nf.end(); ++it) {
        if ( it->second > THRESHOLD / NETFLOW_SAMPLING_RATE) {
            nt_resultHelper.nf_result[it->first] = it->second;
        }
    }
}

void nt_univmon()
{
    int i;

	vector<Packet_Elem> &packets_gt = nt_detectionHelper.packets_gt;
	map <uint32_t, int> &packetMap_gt = nt_detectionHelper.packetMap_gt;

    vector<countSketch> level_countSketch;
    vector<heavyHitters> level_heavyHitters;

    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_countSketch.push_back(countSketch(i));
        level_heavyHitters.push_back(heavyHitters());
    }

    for(int i=1; i<=SAMPLING_LEVEL; i++) {
        level_countSketch[i].index_hash = level_countSketch[0].index_hash;
        level_countSketch[i].res_hash = level_countSketch[0].res_hash;
    }

    print_or_create_params(level_countSketch);

    // get_countsketch_and_heavyhitter(packets_gt, level_countSketch, level_heavyHitters, OPTION_COUNT);
    get_countsketch_and_heavyhitter64(packets_gt, level_countSketch, level_heavyHitters, OPTION_COUNT);

    // for(i=0; i<=SAMPLING_LEVEL; i++) {
    //     level_countSketch[i].sketch_print();
    // }

    // level_heavyHitters[0].topk_print();

    // vector<topk_elem>::iterator it;

    // for (it = level_heavyHitters[0].topk.begin(); it != level_heavyHitters[0].topk.end(); it++) {
    //     uint32_t feature = it->feature;
    //     int estimate = it->estimate;

    //     if(estimate >  THRESHOLD) {
    //         nt_resultHelper.um_result[feature] = estimate;
    //     }
    // }

    for(int pkt=0; pkt < packets_gt.size(); pkt++) {
        uint64_t feature = (((uint64_t)packets_gt[pkt].src_ip) << 32) | (uint64_t)packets_gt[pkt].dst_ip;
        int estimate = level_countSketch[0].estimate64(feature);
        if(estimate >  1) {
            nt_resultHelper.um64_result[feature] = estimate;
        }
    }
}






