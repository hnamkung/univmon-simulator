#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <list>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include <arpa/inet.h>

#define TIME_INTERVAL 20000 // milliseconds
#define THRESHOLD 5000
#define NETFLOW_SAMPLING_RATE 1000
#define NETFLOW_THRESHOLD   (THRESHOLD/NETFLOW_SAMPLING_RATE)

#include "tcp_syn.h"

#include "../library/library.h"
#include "../library/countSketch.h"
#include "../library/heavyHitters.h"
#include "../library/detectionHelper.h"
#include "../library/resultHelper.h"

using namespace std;

void tcp_syn_ground_truth();
void tcp_syn_netflow();
void tcp_syn_univmon();

detectionHelper tcp_syn_detectionHelper = detectionHelper();
resultHelper tcp_syn_resultHelper = resultHelper();

using namespace std;

void tcp_syn_main(int argc, char* argv[])
{
    if(argc < 3) {
        printf("usage : ./simulator tcp_syn [pcap folder name]\n");
        return;
    }
    char *pcap_folder_name = argv[2];
    char pcap_file_name[100];

    DIR *d;
    struct dirent *dir;
    d = opendir(pcap_folder_name);
    while ((dir = readdir(d)) != NULL)
    {
        char* file_name = dir->d_name;
        if(strstr(file_name, ".pcap") != NULL) {
            cout << "pcap file: " << file_name << endl;
            sprintf(pcap_file_name, "%s/%s", pcap_folder_name, file_name);

            tcp_syn_detectionHelper.reset();
            tcp_syn_detectionHelper.pcap_file_name = pcap_file_name;

            tcp_syn_resultHelper.reset();
            tcp_syn_resultHelper.push_back_latency_result(pcap_file_name);


            tcp_syn_ground_truth();
            tcp_syn_netflow();
            tcp_syn_univmon();

            //tcp_syn_resultHelper.print_result();
            // temporary
        }
    }
    // tcp_syn_resultHelper.print_latency();
    closedir(d);
}

#define SYN_ATTEMPT_THRESHOLD (100)
#define SYN_RATIO_THRESHOLD (50)

/**
 * @brief Check whether there is asymmetric between syn and ack packets
 * @param syn_cnt Number of syn packets
 * @param ack_cnt Number of ack packets
 * @return 0 if not asymmetric
 *         1 if src is heavier
 *         2 if dst is heavier.
 */
uint32_t is_asymmetric_ratio (int syn_cnt, int ack_cnt) {
    // handle divide by zero situation
    if (syn_cnt == 0 || ack_cnt == 0) {
        if (syn_cnt >= SYN_ATTEMPT_THRESHOLD) {
            return 1;
        } else if (ack_cnt >= SYN_ATTEMPT_THRESHOLD) {
            return 2;
        } else {
            return 0;
        }
    }
    // check whether their ratio exceeds limit
    if ((syn_cnt / ack_cnt) >= SYN_RATIO_THRESHOLD) {
        return 1;
    } else if ((ack_cnt / syn_cnt) >= SYN_RATIO_THRESHOLD) {
        return 2;
    } else {
        return 0;
    }
}

#define SYN_DIFF_THRESHOLD (1)
uint32_t is_asymmetric_diff (int syn_cnt, int ack_cnt) {
    int diff = syn_cnt - ack_cnt;
    if (diff < (0 - SYN_DIFF_THRESHOLD)) {
        return 2;
    } else if (diff > SYN_DIFF_THRESHOLD) {
        return 1;
    } else {
        return 0;
    }
}

void tcp_syn_ground_truth()
{
    vector<Packet_Elem> &packets_syn_gt = tcp_syn_detectionHelper.packets0_gt;
    map <uint32_t, int> &packetMap_syngt = tcp_syn_detectionHelper.packetMap0_gt;
    set <uint32_t> victims_gt;

    ParseConfig pconfig_syn = ParseConfig(TCP_FLAG_SYN, TCP_FLAG_ACK, 6, true);
    pcapParse(tcp_syn_detectionHelper.get_file_name(), packets_syn_gt, 1, packetMap_syngt, OPTION_COUNT, pconfig_syn);

    map <uint32_t, int>::iterator it;
    for (it = packetMap_syngt.begin(); it != packetMap_syngt.end(); ++it) {
        if(it->second > THRESHOLD) {
            tcp_syn_resultHelper.gt_result[it->first] = it->second;
        }
    }
    // tcp_syn_resultHelper.gt_print();
}

void tcp_syn_netflow()
{
    vector<Packet_Elem> &packets_nf = tcp_syn_detectionHelper.packets_nf;
    map <uint32_t, int> &packetMap_nf = tcp_syn_detectionHelper.packetMap_nf;

    ParseConfig pconfig_syn = ParseConfig(TCP_FLAG_SYN, TCP_FLAG_ACK, 6, true);
    pcapParse(tcp_syn_detectionHelper.get_file_name(), packets_nf, NETFLOW_SAMPLING_RATE, packetMap_nf, OPTION_COUNT, pconfig_syn);

    map <uint32_t, int>::iterator it;
    for (it = packetMap_nf.begin(); it != packetMap_nf.end(); ++it) {
        if ( it->second > THRESHOLD / NETFLOW_SAMPLING_RATE) {
            tcp_syn_resultHelper.nf_result[it->first] = it->second;
        }
    }
    // tcp_syn_resultHelper.nf_print();

    cout << "[Netflow] Packets count: " << packets_nf.size() << endl;
    // Measure the latency

    packetMap_nf.clear();
    uint64_t start_time = packets_nf[0].timestamp;
    uint64_t prev_time = start_time;
    uint64_t cur_time = start_time;
    int detect_count = 0;
    for(int pkt = 0; pkt < packets_nf.size(); pkt ++) {
        cur_time = packets_nf[pkt].timestamp;
        if(cur_time - prev_time >= TIME_INTERVAL) {
            // cout << "[Netflow] New second: " << prev_time << "--" << cur_time << endl;
            packetMap_nf.clear();
            prev_time = cur_time;
            cout << "[Time over]" << endl;
            cout << "latency: " << cur_time - prev_time << "ms" << endl;
            tcp_syn_resultHelper.latency_result.back().nf_latency_vector.push_back(cur_time - prev_time);
            continue;
        }
        Packet_Elem p = packets_nf[pkt];
        map <uint32_t, int>::iterator it;
        it = packetMap_nf.find(p.dst_ip);

        int new_size = 0;
        if (it == packetMap_nf.end()) {
            new_size = 1;
        } else {
            new_size = packetMap_nf.find(p.dst_ip)->second + 1;
        }

        if(new_size > NETFLOW_THRESHOLD && p.dst_ip == 185270273) {
            packetMap_nf.clear();
            detect_count ++;
            // cout << "[Netflow " << detect_count << "] DDoS detected: " << p.dst_ip << ", " << new_size << endl;
            // cout << prev_time << "--" << cur_time << ", ";
            cout << "latency: " << cur_time - prev_time << "ms" << endl;
            tcp_syn_resultHelper.latency_result.back().nf_latency_vector.push_back(cur_time - prev_time);
            prev_time = cur_time;
        } else {
            packetMap_nf[p.dst_ip] = new_size;
        }
    }
}

void tcp_syn_univmon_old()
{
    vector<Packet_Elem> &packets_gt = tcp_syn_detectionHelper.packets0_gt;
    map <uint32_t, int> &packetMap_gt = tcp_syn_detectionHelper.packetMap0_gt;

    vector<countSketch> level_countSketch;
    vector<heavyHitters> level_heavyHitters;

    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_countSketch.push_back(countSketch());
        level_heavyHitters.push_back(heavyHitters());
    }

    cout << endl << endl << "[Univmon] Packets count: " << packets_gt.size() << endl;
    get_countsketch_and_heavyhitter_latency(packets_gt, level_countSketch, level_heavyHitters, OPTION_COUNT, tcp_syn_resultHelper);
}

void tcp_syn_univmon()
{
    vector<Packet_Elem> &packets_gt = tcp_syn_detectionHelper.packets0_gt;
    map <uint32_t, int> &packetMap_gt = tcp_syn_detectionHelper.packetMap0_gt;

    // local variables
    vector<countSketch> level_countSketch0;
    vector<countSketch> level_countSketch1;
    vector<heavyHitters> level_heavyHitters0;
    vector<heavyHitters> level_heavyHitters1;
    // configurations
    PktFilterConfig pkt_syn = PktFilterConfig(TCP_FLAG_SYN, TCP_FLAG_ACK, KEEP);
    PktFilterConfig pkt_ack = PktFilterConfig(TCP_FLAG_ACK, TCP_FLAG_SYN, KEEP);
    // install count sketch and heavy hitters
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_countSketch0.push_back(countSketch());
        level_countSketch1.push_back(countSketch());
        level_heavyHitters0.push_back(heavyHitters());
        level_heavyHitters1.push_back(heavyHitters());
    }
    //  test
    cout << endl << endl << "[Univmon] Packets count: " << packets_gt.size() << endl;
    heavyHitters result = get_countsketch_and_heavyhitter_unbalance(
        level_countSketch0, level_countSketch1,
        level_heavyHitters0, level_heavyHitters1,
        pkt_syn, pkt_ack,
        packets_gt, 100, OPTION_COUNT);
    
}
