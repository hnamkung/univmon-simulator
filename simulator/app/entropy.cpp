#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <list>
#include <set>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <math.h>

#include "basic_flooding.h"

#include "../library/library.h"
#include "../library/countSketch.h"
#include "../library/heavyHitters.h"
#include "../library/detectionHelper.h"
#include "../library/resultHelper.h"

/*
#define TIME_INTERVAL 1000 // milliseconds
//#define THRESHOLD (6.25 * 1024 * 1024) // 6.25MB/s 50Mbps
#define THRESHOLD (1 * 1024 * 1024) // 1MB
#define NETFLOW_SAMPLING_RATE 1000

#define NETFLOW_THRESHOLD   (THRESHOLD/NETFLOW_SAMPLING_RATE)
*/

using namespace std;

void entropy_ground_truth();

void entropy_netflow_uniform();
void entropy_netflow_random();
void entropy_netflow_sh();

void entropy_univmon();
void entropy_univmon_two_indep();
void entropy_univmon_five_indep();

void entropy_sketch();

// gt : ground truth
// nf : netflow
// um : univ mon

detectionHelper entropy_detectionHelper;
resultHelper entropy_resultHelper;

void entropy_main(int argc, char* argv[])
{
    if(argc < 3) {
        printf("usage : ./simulator basic_flooding [pcap folder name]\n");
        return;
    }

    printf("entropy!\n");

    char *pcap_folder_name = argv[2];
    char pcap_file_name[100];

    mkdir(pcap_folder_name, 0777);

    DIR *d;
    struct dirent *dir;
    d = opendir(pcap_folder_name);

    while ((dir = readdir(d)) != NULL)
    {
        char* file_name = dir->d_name;
        if(strstr(file_name, ".pcap") != NULL) {
            sprintf(pcap_file_name, "%s/%s", pcap_folder_name, file_name);
            cout << pcap_file_name << endl;
            entropy_detectionHelper.reset();
            entropy_detectionHelper.pcap_file_name = pcap_file_name;

            entropy_resultHelper.reset();

            // struct Latency_Elem lm;
            // string str(pcap_file_name);
            // lm.pcap_name = str;
            // entropy_resultHelper.latency_result.push_back(lm);

            entropy_ground_truth();

            // entropy_netflow_uniform();
            // entropy_netflow_random();
            // entropy_netflow_sh(); // sample and hold
            
            entropy_univmon_two_indep();
            entropy_univmon_five_indep();

            entropy_sketch();

            // entropy_resultHelper.print_result();
            // temporary
        }
    }
    
    // basic_resultHelper.print_latency();

    closedir(d);
}

void calculate_entropy(map <uint32_t, int> &packetMap, double xlogx)
{
    int m=0;
    double f=0;
    int fi;

    map <uint32_t, int>::iterator it;
    for (it = packetMap.begin(); it != packetMap.end(); ++it) {
        fi = it->second;

        f += fi * log2(fi);

        m += fi;
    }

    printf("entropy : %f\n\n", log2(m) - (f/m));
    if(xlogx != 0) {
        // printf("xlogx : %f\n", xlogx);
        printf("entropy xlogx : %f\n\n", log2(m) - (xlogx/m));
    }
}

void entropy_ground_truth()
{
    vector<Packet_Elem> &packets_gt = entropy_detectionHelper.packets_gt;
    map <uint32_t, int> &packetMap_gt = entropy_detectionHelper.packetMap_gt;

    ParseConfig pconfig = ParseConfig();
    pcapParse(entropy_detectionHelper.get_file_name(), packets_gt, 1, packetMap_gt, OPTION_COUNT, pconfig);

    // calculate_entropy(packetMap_gt, 0);
}

void entropy_netflow_uniform()
{
    vector<Packet_Elem> &packets_nf = entropy_detectionHelper.packets_nf;
    map <uint32_t, int> &packetMap_nf = entropy_detectionHelper.packetMap_nf;
    NetflowConfig nf_config = NetflowConfig(Uniform_Sampling, 1000);
    pcapParse_netflow(entropy_detectionHelper.get_file_name(), packets_nf, packetMap_nf, OPTION_COUNT, nf_config);

    calculate_entropy(packetMap_nf, 0);

    packets_nf.clear();
    packetMap_nf.clear();
}

void entropy_netflow_random()
{
    vector<Packet_Elem> &packets_nf = entropy_detectionHelper.packets_nf;
    map <uint32_t, int> &packetMap_nf = entropy_detectionHelper.packetMap_nf;
    NetflowConfig nf_config = NetflowConfig(Random_Sampling, 1000);
    pcapParse_netflow(entropy_detectionHelper.get_file_name(), packets_nf, packetMap_nf, OPTION_COUNT, nf_config);

    calculate_entropy(packetMap_nf, 0);

    packets_nf.clear();
    packetMap_nf.clear();
}

void entropy_netflow_sh()
{
    vector<Packet_Elem> &packets_nf = entropy_detectionHelper.packets_nf;
    map <uint32_t, int> &packetMap_nf = entropy_detectionHelper.packetMap_nf;
    NetflowConfig nf_config = NetflowConfig(Sample_and_Hold, 1000);
    pcapParse_netflow(entropy_detectionHelper.get_file_name(), packets_nf, packetMap_nf, OPTION_COUNT, nf_config);

    calculate_entropy(packetMap_nf, 0);

    packets_nf.clear();
    packetMap_nf.clear();
}

void entropy_univmon_two_indep()
{
    h_hash pt;
    pt.global_hash_type = Alan_Hash;

    vector<Packet_Elem> &packets_gt = entropy_detectionHelper.packets_gt;
    map <uint32_t, int> &packetMap_gt = entropy_detectionHelper.packetMap_gt;

    vector<countSketch> level_countSketch;
    vector<heavyHitters> level_heavyHitters;

    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_countSketch.push_back(countSketch());
        level_heavyHitters.push_back(heavyHitters());
    }

    printf("[Univmon 2-indep]\n");

    get_countsketch_and_heavyhitter(packets_gt, level_countSketch, level_heavyHitters, OPTION_COUNT);
    double xlogx = get_entropy(level_countSketch, level_heavyHitters);

    calculate_entropy(packetMap_gt, xlogx);
}

void entropy_univmon_five_indep()
{
    h_hash pt;
    pt.global_hash_type = Alan_Hash;
    // pt.global_hash_type = Tabular5_Hash;

    vector<Packet_Elem> &packets_gt = entropy_detectionHelper.packets_gt;
    map <uint32_t, int> &packetMap_gt = entropy_detectionHelper.packetMap_gt;

    vector<countSketch> level_countSketch;
    vector<heavyHitters> level_heavyHitters;

    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_countSketch.push_back(countSketch());
        level_heavyHitters.push_back(heavyHitters());
    }

    printf("[Univmon 5-indep]\n");

    get_countsketch_and_heavyhitter(packets_gt, level_countSketch, level_heavyHitters, OPTION_COUNT);
    double xlogx = get_entropy(level_countSketch, level_heavyHitters);

    calculate_entropy(packetMap_gt, xlogx);
}

class FlowKey
{
    public:
    uint32_t srcip;
    uint32_t dstip;
    uint32_t srcport;
    uint32_t dstport;
    uint8_t proto;
    FlowKey()
    {
        srcip =0;
        dstip =0;
        srcport=0;
        dstport=0;
        proto=0;
    }
    FlowKey(uint32_t src)
    {
        srcip = src;
        dstip =0;
        srcport=0;
        dstport=0;
        proto=0;
    }
    FlowKey(uint32_t src, uint32_t dst)
    {
        srcip = src;
        dstip = dst;
        srcport=0;
        dstport=0;
        proto=0;
    }
    FlowKey(uint32_t sa, uint32_t da, uint32_t sp, uint32_t dp, uint8_t p)
    {
        srcip =sa;
        dstip =da;
        srcport = sp;
        dstport = dp;
        proto =p;   
    }
    double get_value() const 
    {
    //  return (double)((double)proto + (double)dstport * pow(2,sizeof(proto)) + (double)srcport * pow(2,sizeof(proto) + sizeof(srcport)) + (double)dstip *  pow(2,sizeof(proto) + sizeof(dstport) + sizeof(srcport)) +  (double)srcip *  pow(2, sizeof(proto) + sizeof(dstport) + sizeof(srcport) + sizeof(dstip)) ) ; 
        return (proto + dstport + srcport + dstip + srcip);
    }   
    bool operator<( const FlowKey &a) const
    {
        int value1 = proto + dstport + srcport + dstip + srcip;
        int value2 = a.proto + a.dstport + a.srcport + a.dstip + a.srcip;

        //return (get_value() < a.get_value());
        return (value1 < value2);
    }
};

double estimator_function(unsigned int x)
{
    if (x == 0)
        return 0;

    return ((double)x) * log2((double)x);
}

int compare(const void * a, const void * b)
{
    if ( (*(double*)a - *(double*)b) > 0)
        return 1;
    else if ( (*(double*)a - *(double*)b) < 0)
        return -1;

    return 0;
}

void entropy_sketch()
{
    printf("[AMS sketch start]\n");

    vector<Packet_Elem> &packets_gt = entropy_detectionHelper.packets_gt;
    map <uint32_t, int> &packetMap_gt = entropy_detectionHelper.packetMap_gt;

    set<unsigned int> locations_to_sample;
    unsigned int s1;
    unsigned int s2;    
    unsigned int numpkts = packets_gt.size();
    double epsilon = 0.3;
    double delta = 0.3;

    multimap<FlowKey, unsigned int > flowkey2counterindices;
    vector<unsigned int> counters;
    unsigned pktindex = 0;

    counters.clear();
    flowkey2counterindices.clear();


    s1 = (unsigned int)(0.99 + (8*log2((double)numpkts) + 32) / (epsilon*epsilon)) ;
    s2 = (unsigned int)(0.99 + 2 * log2(1.0/delta));

    printf("s1 : %d\n", s1);
    printf("s2 : %d\n", s2);
    printf("s1*s2 : %d\n", s1*s2);

    unsigned int numselected = 0;
    while (numselected < (s1*s2))
    {
        unsigned int currentlocation = random() % numpkts;
        if (locations_to_sample.find(currentlocation) == locations_to_sample.end())
        {
            locations_to_sample.insert(currentlocation);
            numselected++;
        }
    }

    for(int pkt=0; pkt < packets_gt.size(); pkt++) {
        uint32_t dst = packets_gt[pkt].dst_ip;
        FlowKey f = FlowKey(dst);

        if (flowkey2counterindices.find(f) !=  flowkey2counterindices.end() )
        {
            pair<multimap<FlowKey, unsigned int>::iterator, multimap<FlowKey, unsigned int>::iterator> ppp;

            ppp = flowkey2counterindices.equal_range(f);

            for (multimap<FlowKey, unsigned int>::iterator it2 = ppp.first;    it2 != ppp.second;   ++it2)
            {
                unsigned counter_index =  (*it2).second ;
                counters[counter_index] = counters[counter_index] + 1;
            }
        }
        
        if (locations_to_sample.find(pktindex) != locations_to_sample.end())
        {
            unsigned curmax = counters.size();
            unsigned int freshcounter = 1;
            counters.push_back(freshcounter);
            flowkey2counterindices.insert(pair<FlowKey,unsigned int>(f,curmax));
        }
        pktindex++;
    }

    // estimate
    double* avg = new double[s2];
    double answer = 0;

    for(int i = 0; i < s2; ++i)
    {
        double sum = 0;
        for(int j = 0; j < s1; ++j)
        {
            double estimate = numpkts * (estimator_function(counters[i * s1 + j]) - estimator_function(counters[i * s1 + j]-1));
            sum += estimate;
        }
        avg[i] = sum / s1;
    }
    
    // sort the averages
    qsort(avg, s2, sizeof(double), compare);

    if (s2 % 2 == 1)
        answer = avg[s2/2];  
    else
        answer = (avg[s2/2-1]); // for even s2 take the lower median

    delete [] avg;
    // printf("sketch entropy : %f\n", answer);
    calculate_entropy(packetMap_gt, answer);
}
