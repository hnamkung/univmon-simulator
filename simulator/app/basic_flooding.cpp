#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <list>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>

#include "basic_flooding.h"

#include "../library/library.h"
#include "../library/countSketch.h"
#include "../library/heavyHitters.h"
#include "../library/detectionHelper.h"
#include "../library/resultHelper.h"

#define TIME_INTERVAL 1000 // milliseconds
//#define THRESHOLD (6.25 * 1024 * 1024) // 6.25MB/s 50Mbps
#define THRESHOLD (1 * 1024 * 1024) // 1MB
#define NETFLOW_SAMPLING_RATE 1000

#define NETFLOW_THRESHOLD   (THRESHOLD/NETFLOW_SAMPLING_RATE)

using namespace std;

void basic_ground_truth();
void basic_netflow();
void basic_univmon();

// gt : ground truth
// nf : netflow
// um : univ mon

detectionHelper basic_detectionHelper;
resultHelper basic_resultHelper;

void basic_flooding_main(int argc, char* argv[])
{
    if(argc < 3) {
        printf("usage : ./simulator basic_flooding [pcap folder name]\n");
        return;
    }

    char *pcap_folder_name = argv[2];
    char pcap_file_name[100];
 
    DIR *d;
    struct dirent *dir;
    d = opendir(pcap_folder_name);
    while ((dir = readdir(d)) != NULL)
    {
        char* file_name = dir->d_name;
        if(strstr(file_name, ".pcap") != NULL) {
            sprintf(pcap_file_name, "%s/%s", pcap_folder_name, file_name);
            cout << pcap_file_name << endl;
            basic_detectionHelper.reset();
            basic_detectionHelper.pcap_file_name = pcap_file_name;

            basic_resultHelper.reset();

            struct Latency_Elem lm;
            string str(pcap_file_name);
            lm.pcap_name = str;
            basic_resultHelper.latency_result.push_back(lm);

            basic_ground_truth();
            basic_netflow();
            basic_univmon();

            // basic_resultHelper.print_result();
            // temporary
        }
    }
    basic_resultHelper.print_latency();
    closedir(d);
}

void basic_ground_truth()
{
    vector<Packet_Elem> &packets_gt = basic_detectionHelper.packets_gt;
    map <uint32_t, int> &packetMap_gt = basic_detectionHelper.packetMap_gt;

    ParseConfig pconfig = ParseConfig();
    pcapParse(basic_detectionHelper.get_file_name(), packets_gt, 1, packetMap_gt, OPTION_SIZE, pconfig);

    map <uint32_t, int>::iterator it;
    for (it = packetMap_gt.begin(); it != packetMap_gt.end(); ++it) {
        if ( it->second > THRESHOLD) {
            basic_resultHelper.gt_result[it->first] = it->second;
        }
    }
    // basic_resultHelper.gt_print();
}

void basic_netflow()
{
    vector<Packet_Elem> &packets_nf = basic_detectionHelper.packets_nf;
    map <uint32_t, int> &packetMap_nf = basic_detectionHelper.packetMap_nf;

    ParseConfig pconfig = ParseConfig();
    pcapParse(basic_detectionHelper.get_file_name(), packets_nf, NETFLOW_SAMPLING_RATE, packetMap_nf, OPTION_SIZE, pconfig);

    map <uint32_t, int>::iterator it;
    for (it = packetMap_nf.begin(); it != packetMap_nf.end(); ++it) {
        if ( it->second > THRESHOLD / NETFLOW_SAMPLING_RATE) {
            basic_resultHelper.nf_result[it->first] = it->second;
        }
    }
    // basic_resultHelper.nf_print();

    cout << "[Netflow] Packets count: " << packets_nf.size() << endl;
    // Measure the latency
    packetMap_nf.clear();
    uint64_t start_time = packets_nf[0].timestamp;
    uint64_t prev_time = start_time;
    uint64_t cur_time = start_time;
    int detect_count = 0;
    for(int pkt = 0; pkt < packets_nf.size(); pkt ++) {
        cur_time = packets_nf[pkt].timestamp;
        if(cur_time - prev_time >= TIME_INTERVAL) {
            // cout << "[Netflow] New second: " << prev_time << "--" << cur_time << endl;
            packetMap_nf.clear();
            prev_time = cur_time;
            cout << "[Time over]" << endl;
            cout << "latency: " << cur_time - prev_time << "ms" << endl;
            basic_resultHelper.latency_result.back().nf_latency_vector.push_back(cur_time - prev_time);
            continue;
        }
        Packet_Elem p = packets_nf[pkt];
        map <uint32_t, int>::iterator it;
        it = packetMap_nf.find(p.dst_ip);
        int new_size;
        if (it == packetMap_nf.end()) {
            new_size = p.size;
        } else {
            new_size = packetMap_nf.find(p.dst_ip)->second + p.size;
        }
        if(new_size > NETFLOW_THRESHOLD && p.dst_ip == 185270273) {
            packetMap_nf.clear();
            detect_count ++;
            // cout << "[Netflow " << detect_count << "] DDoS detected: " << p.dst_ip << ", " << new_size << endl;
            // cout << prev_time << "--" << cur_time << ", ";
            cout << "latency: " << cur_time - prev_time << "ms" << endl;
            basic_resultHelper.latency_result.back().nf_latency_vector.push_back(cur_time - prev_time);
            prev_time = cur_time;
        } else {
            packetMap_nf[p.dst_ip] = new_size;
        }
    }
}

void basic_univmon()
{
    vector<Packet_Elem> &packets_gt = basic_detectionHelper.packets_gt;
    map <uint32_t, int> &packetMap_gt = basic_detectionHelper.packetMap_gt;

    vector<countSketch> level_countSketch;
    vector<heavyHitters> level_heavyHitters;

    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_countSketch.push_back(countSketch());
        level_heavyHitters.push_back(heavyHitters());
    }

    cout << endl << endl << "[Univmon] Packets count: " << packets_gt.size() << endl;
    get_countsketch_and_heavyhitter_latency(packets_gt, level_countSketch, level_heavyHitters, OPTION_SIZE, basic_resultHelper);

    vector<topk_elem>::iterator it;

    int hh_idx = 0;
    for (it = level_heavyHitters[hh_idx].topk.begin(); it != level_heavyHitters[hh_idx].topk.end(); it++) {
        uint32_t feature = it->feature;
        int estimate = it->estimate;

        if(estimate >  THRESHOLD) {
            basic_resultHelper.um_result[feature] = estimate;
        }
    }
}


