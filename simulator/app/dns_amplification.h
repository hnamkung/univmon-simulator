#ifndef __DNS_AMPLIFICATION_H

#define __DNS_AMPLIFICATION_H

/**
 * Inteface for detecting DNS amplification attack. 
 */
void dns_amplification_main(int argc, char* argv[]);

#endif
