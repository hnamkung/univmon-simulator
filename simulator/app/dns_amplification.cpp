#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <list>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>


#include "dns_amplification.h"

#include "../library/library.h"
#include "../library/countSketch.h"
#include "../library/heavyHitters.h"
#include "../library/detectionHelper.h"
#include "../library/resultHelper.h"

using namespace std;


void dns_amp_query_univmon();

detectionHelper dns_amp_query_detectionHelper = detectionHelper();
resultHelper dns_amp_query_resultHelper = resultHelper();

void dns_amplification_main(int argc, char* argv[])
{
    if(argc < 4) {
        printf("usage : ./simulator dns_amp [query pcap folder] [response pcap folder]\n");
        return;
    }

    char *query_pcap_folder_name = argv[2];
    char *resp_pcap_folder_name = argv[3];
    cout << query_pcap_folder_name << endl;
    cout << resp_pcap_folder_name << endl;

    char pcap_file_name[100];

    int pcap_count = 0;

    DIR *d;
    struct dirent *dir;
    d = opendir(query_pcap_folder_name);
    while ((dir = readdir(d)) != NULL)
    {
        char* file_name = dir->d_name;
        if(strstr(file_name, ".pcap") != NULL) {
            sprintf(pcap_file_name, "%s/%s", query_pcap_folder_name, file_name);
            cout << pcap_file_name << endl;
            dns_amp_query_detectionHelper.pcap_file_name = pcap_file_name;

            dns_amp_query_detectionHelper.reset();
            dns_amp_query_resultHelper.reset();

            dns_amp_query_univmon();

            // temporary
            break;
        }

    }
    closedir(d);
}

void dns_amp_query_univmon()
{
    vector<Packet_Elem> &packets_gt = dns_amp_query_detectionHelper.packets_gt;
    map <uint32_t, int> &packetMap_gt = dns_amp_query_detectionHelper.packetMap_gt;

    ParseConfig pconfig = ParseConfig();
    pcapParse(dns_amp_query_detectionHelper.get_file_name(),
            packets_gt, 1, packetMap_gt, OPTION_COUNT, pconfig);


    // sketch0 for dns queries and sketch1 for dns responses
    vector<countSketch> level_countSketch0;
    vector<countSketch> level_countSketch1;
    vector<heavyHitters> level_heavyHitters0;
    vector<heavyHitters> level_heavyHitters1;

    // configurations
    PktFilterConfig pkt_dns_req = PktFilterConfig(KEEP);
    // destination port of DNS request is 53
    pkt_dns_req.set_dport(53);
    PktFilterConfig pkt_dns_resp = PktFilterConfig(KEEP);
    // source port of DNS response is 53
    pkt_dns_resp.set_sport(53);


    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_countSketch0.push_back(countSketch(FEATURE_SRCIP));
        level_countSketch1.push_back(countSketch(FEATURE_DESTIP));
        level_heavyHitters0.push_back(heavyHitters());
        level_heavyHitters1.push_back(heavyHitters());
    }

    cout << endl << endl << "[Univmon] Packets count: " << packets_gt.size() << endl;
    heavyHitters result = get_countsketch_and_heavyhitter_unbalance(
        level_countSketch0, level_countSketch1,
        level_heavyHitters0, level_heavyHitters1,
        pkt_dns_req, pkt_dns_resp,
        packets_gt, 100, OPTION_COUNT);
    vector<topk_elem>::iterator it;

    // int hh_idx = 0;
    // for (it = level_heavyHitters[hh_idx].topk.begin(); it != level_heavyHitters[hh_idx].topk.end(); it++) {
    //     uint32_t feature = it->feature;
    //     int estimate = it->estimate;

    //     if(estimate >  THRESHOLD) {
    //         basic_resultHelper.um_result[feature] = estimate;
    //     }
    // }

}

// void dns_amp_response_univmon()
// {

// }
