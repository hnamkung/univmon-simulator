#include "heavyHitters.h"

#include "library.h"

using namespace std;

heavyHitters::heavyHitters()
{
}


void heavyHitters::topk_print()
{
    int c=0;
	vector<topk_elem>::iterator it;
    for (it = topk.begin(); it != topk.end(); it++) {
    	c++;
        printf("%3d] %20s (%12u) change : %d\n", c, get_ip_string_from_int(it->feature).c_str(), it->feature, it->estimate);
    }
}

void heavyHitters::sort()
{
	sort_heap(topk.begin(), topk.end(), topk_elem_cmp());
}

bool heavyHitters::full()
{
	if(topk.size() >= topk_number)
		return true;
	return false;
}

int heavyHitters::get_min()
{
	return topk.front().estimate;
}

void heavyHitters::insert(uint32_t feature, int estimate)
{
	struct topk_elem lm;
	lm.feature = feature;
	lm.estimate = estimate;
	topk.push_back(lm);
    push_heap (topk.begin(), topk.end(), topk_elem_cmp());
}

void heavyHitters::pop()
{
    pop_heap(topk.begin(),topk.end(), topk_elem_cmp());
    topk.pop_back();
}

void heavyHitters::update_or_insert_or_replace(uint32_t feature, int estimate)
{
	vector<topk_elem>::iterator it;
    for (it = topk.begin(); it != topk.end(); it++) {
    	if(it->feature == feature) {
    		it->estimate = estimate;
    		make_heap(topk.begin(), topk.end(), topk_elem_cmp());
    		return;
    	}
    }
    if(full())
	    pop();
	insert(feature, estimate);
}

// this function assumes that elements update in heap monotonically increases 
void heavyHitters::topk_update(uint32_t feature, int estimate)
{
	if(full() && get_min() > estimate) {
		return;
	}

	update_or_insert_or_replace(feature, estimate);
}

// this function assumes that elements update in heap can have decreased value
void heavyHitters::topk_update_change(uint32_t feature, int estimate)
{
	vector<topk_elem>::iterator it;
    for (it = topk.begin(); it != topk.end(); it++) {
    	if(it->feature == feature) {
    		it->estimate = estimate;
    		make_heap(topk.begin(), topk.end(), topk_elem_cmp());
    		return;
    	}
    }
    if(!full()) {
    	insert(feature, estimate);
    }
    else {
    	if(get_min() < estimate) {
	    	pop();
	    	insert(feature, estimate);
		}
    }
}

void heavyHitters::reset()
{
    topk.clear();
}

