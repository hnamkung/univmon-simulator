#ifndef __CS_H

#define __CS_H

#include <vector>
#include <stdlib.h>
#include "heavyHitters.h"
#include "h_hash.h"

#define COUNT_SKETCH_WIDTH 1024
#define COUNT_SKETCH_ROW 5

using namespace std;

enum FeatureType {
    FEATURE_DESTIP,
    FEATURE_SRCIP
};

class countSketch {

public:
	static const int width = COUNT_SKETCH_WIDTH;
	static const int row = COUNT_SKETCH_ROW;

	int* sketchArray;

	h_hash sampling_hash;
	vector<h_hash> index_hash;
	vector<h_hash> res_hash;

    FeatureType feature_type;

    countSketch();
    countSketch(int level);
    countSketch(FeatureType type);

    void sketch(uint32_t feature, int elem);
    void sketch64(uint64_t feature, int elem);

	int estimate(uint32_t feature);
    int estimate64(uint64_t feature);

	void sketch_print();
    void reset();

    void set_feature_type(FeatureType type);

private:

};

#endif
