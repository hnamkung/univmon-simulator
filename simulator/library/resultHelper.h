#ifndef __RESULT_H

#define __RESULT_H

#include <map>
#include <vector>
#include "stdint.h"

using namespace std;


struct Eval_Result {
    int true_positive;
    int false_negative;
    int false_positive;
    int true_negative;

    double precision;
    double recall;
    double f1_score;
};

struct Latency_Elem {
    string pcap_name;
    vector<uint64_t> nf_latency_vector;
    vector<uint64_t> um_latency_vector;
};

class resultHelper {

public:
    void reset();
    map<uint32_t, int> gt_result;
    map<uint32_t, int> nf_result;
    map<uint32_t, int> um_result;
    map<uint64_t, int> um64_result;


    void print_result();

	void print_detection_result(map<uint32_t, int> result);
    void print_detection_result64(map<uint64_t, int> result);
    void gt_print();
	void nf_print();
	void um_print();
    void um64_print();

    vector<Latency_Elem> latency_result;
    void push_back_latency_result(char* pcap_file_name);
    void print_latency();

};

#endif
