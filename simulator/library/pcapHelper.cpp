#include <iostream>
#include <vector>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <pcap.h>
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>

#include "pcapHelper.h"

using namespace std;

// from attackGenerator/library/pcapHelper.h
struct hun_tcphdr {
    u_int16_t   source;
    u_int16_t   dest;
    u_int32_t   seq;
    u_int32_t   ack_seq;
    u_int16_t   res1:4,
        doff:4,
        fin:1,
        syn:1,
        rst:1,
        psh:1,
        ack:1,
        urg:1,
        ece:1,
        cwr:1;
    u_int16_t   window;
    u_int16_t   check;
    u_int16_t   urg_ptr;
};

vector<Packet_Elem> *global_packets;
map<uint32_t, int> *global_packetMap;
// flags to be checked
uint16_t flagset, flagnot;
int proto_filter;

int sampling_rate, count_or_size;
bool is_dest; // whether dst or src

int packet_count;

// get tcp flags from pcap
int pcapGetTcpFlags(const u_char* packet) {
    const struct ip* ipHeader = (struct ip*)(packet+14);
    const struct hun_tcphdr* tcpHeader = (struct hun_tcphdr*) (packet+14+(ipHeader->ip_hl * 4));
    // cout << "ip header len: " << ipHeader->ip_hl << endl;
    // just take care of the following flags
    int flags = 0;
    flags |= tcpHeader->urg == 1 ? TCP_FLAG_URG : 0;
    flags |= tcpHeader->ack == 1 ? TCP_FLAG_ACK : 0;
    flags |= tcpHeader->psh == 1 ? TCP_FLAG_PSH : 0;
    flags |= tcpHeader->rst == 1 ? TCP_FLAG_RST : 0;
    flags |= tcpHeader->syn == 1 ? TCP_FLAG_SYN : 0;
    flags |= tcpHeader->fin == 1 ? TCP_FLAG_FIN : 0;
    // printf("urg: %d, ack: %d, psh: %d, rst: %d, syn: %d, fin: %d\n",
        // tcpHeader->urg,tcpHeader->ack,tcpHeader->psh,tcpHeader->rst,tcpHeader->syn,tcpHeader->fin);
    // printf("flags: %x\n", flags);
    return flags;
}

int pcapGetProto(const u_char* packet) {
    const struct ip* ipHeader = (struct ip*)(packet+14);
    return ipHeader->ip_p;
}

bool checkFlags(int toCheck) {
    if (!CHK_TCP_FALGS_SET(toCheck, flagset)) {
        return false;
    }
    if (CHK_TCP_FLAGS_HASSET(toCheck, flagnot)) {
        return false;
    }
    return true;
}

void packetHandler(const struct pcap_pkthdr* pkthdr, const u_char* packet)
{
    const struct ether_header* ethernetHeader;
    const struct ip* ipHeader;

    char sourceIp[INET_ADDRSTRLEN];
    char destIp[INET_ADDRSTRLEN];

    map <uint32_t, int>::iterator it;


    ethernetHeader = (struct ether_header*)packet;

    if(ntohs(ethernetHeader->ether_type) != 0x0800) { // IPv4
        return;
    }


    packet_count++;
    if(packet_count % sampling_rate != 0)
        return;

    // filter packets based on flags
    if (proto_filter >= 0 && proto_filter != pcapGetProto(packet)) {
        return;
    }
    if (!checkFlags(pcapGetTcpFlags(packet))) {
        return;
    }

    ipHeader = (struct ip*)(packet+14);

    struct in_addr tmpPkt1, tmpPkt2;

    inet_ntop(AF_INET, &(ipHeader->ip_src), sourceIp, INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &(ipHeader->ip_dst), destIp, INET_ADDRSTRLEN);

    inet_aton(sourceIp,&tmpPkt1);
    inet_aton(destIp,&tmpPkt2);

    // parse and construct Packet_Elem element
    Packet_Elem p;

    p.src_ip = ntohl(ipHeader->ip_src.s_addr);
    p.dst_ip = ntohl(ipHeader->ip_dst.s_addr);

    uint32_t key = p.dst_ip;

    // cout <<   "#src: " << get_ip_string_from_int(p.src_ip)
    //      << ", dst: " << get_ip_string_from_int(p.dst_ip) << endl;

    // cout <<   "*src: " << get_ip_string_from_int(ipHeader->ip_src.s_addr)
    //      << ", dst: " << get_ip_string_from_int(ipHeader->ip_dst.s_addr) << endl;

    get_ip_char_from_int(p.src_ip_addr, p.src_ip);
    get_ip_char_from_int(p.dst_ip_addr, p.dst_ip);


    p.size = ntohs(ipHeader->ip_len) + 14;

    // Get timestamp in milliseconds
    p.timestamp = pkthdr->ts.tv_sec * 1000 + pkthdr->ts.tv_usec / 1000;

    // get protocol
    p.ip_proto = ipHeader->ip_p;

    // Get UDP or TCP specific parameters.
    if(p.ip_proto == IP_PROTO_UDP) {
        const struct udphdr *udp_header = (struct udphdr *)(packet+14+(ipHeader->ip_hl * 4));
        p.src_port = ntohs(udp_header->uh_sport);
        p.dst_port = ntohs(udp_header->uh_dport);
    } else if(p.ip_proto == IP_PROTO_TCP) {
        const struct tcphdr *tcp_header = (struct tcphdr *)(packet+14+(ipHeader->ip_hl * 4));
        p.src_port = ntohs(tcp_header->th_sport);
        p.dst_port = ntohs(tcp_header->th_dport);
        // get flags
        p.tcp_flags = pcapGetTcpFlags(packet);

    } else {
        p.src_port = 0;
        p.dst_port = 0;
    }


    global_packets->push_back(p);

    if (!is_dest) {
        key = p.src_ip;
    }
    it = global_packetMap->find(key);

    // taking care of whether packet count or overall payload size
    int elem = 0;
    if(count_or_size == OPTION_COUNT) {
        elem = 1;
    } else if(count_or_size == OPTION_SIZE) {
        elem = p.size;
    }

    // count occuring times or overall payload size of the dst in stream
    if (it == global_packetMap->end()) {
        (*global_packetMap)[key] = elem;
    }
    else {
        (*global_packetMap)[key] = global_packetMap->find(key)->second + elem;
    }
}

// flagset is the flag that needs to be set
// flagnot is the flag that will be excluded
void pcapParse(string pcapFile, vector<Packet_Elem> &packets, int _sampling_rate, map<uint32_t, int> &packetMap, int _count_or_size, ParseConfig pconfig)
{
    // set global variable to be used by packet handler
    packet_count = 0;

    flagset = pconfig.get_flag_set();
    flagnot = pconfig.get_flag_not();
    proto_filter = pconfig.get_proto();
    is_dest = pconfig.get_is_dest();

    pcap_t *descr;
    char errbuf[PCAP_ERRBUF_SIZE];

	packets.clear();
	packetMap.clear();

    global_packets = &packets;
    global_packetMap = &packetMap;

    sampling_rate = _sampling_rate;
    count_or_size = _count_or_size;

    struct pcap_pkthdr header;
    const u_char *packet;

    descr = pcap_open_offline(pcapFile.c_str(), errbuf);

    while(true) {
        packet = pcap_next(descr, &header);
        if(packet == NULL)
            break;

        packetHandler(&header, packet);
    }

    printf("pcap file reading is done... total %d packets \n", (int)packets.size());
}

void pcapParse_netflow(string pcapFile, vector<Packet_Elem> &packets, map<uint32_t, int> &packetMap, int _count_or_size, NetflowConfig nf_config)
{
    int local_packet_count = 0;
    int local_sampling_count = nf_config.sampling_count;
    char errbuf[PCAP_ERRBUF_SIZE];
    struct pcap_pkthdr header;
    const u_char *packet;
    pcap_t *descr = pcap_open_offline(pcapFile.c_str(), errbuf);

    while(true) {
        packet = pcap_next(descr, &header);
        if(packet == NULL)
            break;

        const struct ether_header* ethernetHeader;
        const struct ip* ipHeader;

        char sourceIp[INET_ADDRSTRLEN];
        char destIp[INET_ADDRSTRLEN];

        map <uint32_t, int>::iterator it;

        ethernetHeader = (struct ether_header*)packet;

        if(ntohs(ethernetHeader->ether_type) != 0x0800) { // IPv4
            continue;
        }

        Packet_Elem p;

        ipHeader = (struct ip*)(packet+14);

        p.src_ip = ntohl(ipHeader->ip_src.s_addr);
        p.dst_ip = ntohl(ipHeader->ip_dst.s_addr);

        uint32_t key = p.dst_ip;

        if(nf_config.type == Uniform_Sampling) {
            local_packet_count++;
            if(local_packet_count % local_sampling_count != 0)
                continue;
        }
        
        else if(nf_config.type == Random_Sampling) {
            if( rand() % local_sampling_count != 0) {
                continue;
            }
        }

        else if(nf_config.type == Sample_and_Hold) {
            if( rand() % local_sampling_count != 0) {
                it = packetMap.find(key);
                if (it == packetMap.end()) {
                    continue;
                }
            }
        }

        struct in_addr tmpPkt1, tmpPkt2;

        inet_ntop(AF_INET, &(ipHeader->ip_src), sourceIp, INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &(ipHeader->ip_dst), destIp, INET_ADDRSTRLEN);

        inet_aton(sourceIp,&tmpPkt1);
        inet_aton(destIp,&tmpPkt2);

        get_ip_char_from_int(p.src_ip_addr, p.src_ip);
        get_ip_char_from_int(p.dst_ip_addr, p.dst_ip);


        p.size = ntohs(ipHeader->ip_len) + 14;

        // Get timestamp in milliseconds
        p.timestamp = header.ts.tv_sec * 1000 + header.ts.tv_usec / 1000;

        // get protocol
        p.ip_proto = ipHeader->ip_p;

        // Get UDP or TCP specific parameters.
        if(p.ip_proto == IP_PROTO_UDP) {
            const struct udphdr *udp_header = (struct udphdr *)(packet+14+(ipHeader->ip_hl * 4));
            p.src_port = ntohs(udp_header->uh_sport);
            p.dst_port = ntohs(udp_header->uh_dport);
        } else if(p.ip_proto == IP_PROTO_TCP) {
            const struct tcphdr *tcp_header = (struct tcphdr *)(packet+14+(ipHeader->ip_hl * 4));
            p.src_port = ntohs(tcp_header->th_sport);
            p.dst_port = ntohs(tcp_header->th_dport);
            // get flags
            p.tcp_flags = pcapGetTcpFlags(packet);

        } else {
            p.src_port = 0;
            p.dst_port = 0;
        }

        packets.push_back(p);

        it = packetMap.find(key);

        // taking care of whether packet count or overall payload size
        int elem = 0;
        if(count_or_size == OPTION_COUNT) {
            elem = 1;
        } else if(count_or_size == OPTION_SIZE) {
            elem = p.size;
        }

        if (it == packetMap.end()) {
            packetMap[key] = elem;
        }
        else {
            packetMap[key] = packetMap.find(key)->second + elem;
        }
    }

    printf("pcap file reading is done. total packets[%d] flowkeys[%d] \n", (int)packets.size(), (int)packetMap.size());
}

void pcap_get_info(char* input_file_name, int &pcap_packet_count, int &pcap_byte_size)
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *pt = pcap_open_offline(input_file_name, errbuf);

    int count = 0;
    int size = 0;

    const struct ether_header* ethernetHeader;
    const struct ip* ipHeader;

    struct pcap_pkthdr header;
    const u_char *packet;
    while(true) {
        packet = pcap_next(pt, &header);
        if(packet == NULL)
            break;

        count ++;

        ethernetHeader = (struct ether_header*)packet;

        if(ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP) { // IPv4
            ipHeader = (struct ip*)(packet+14);
            size += ntohs(ipHeader->ip_len) + 14;
        }
    }
    pcap_close(pt);

    pcap_packet_count = count;
    pcap_byte_size = size;
}

void get_ip_char_from_int(char ip_addr[16], uint32_t ip)
{
    char buf[INET_ADDRSTRLEN];
    uint32_t net_ip = ntohl(ip);
    inet_ntop(AF_INET, &net_ip, buf, INET_ADDRSTRLEN);
    strcpy(ip_addr, buf);
}

string get_ip_string_from_int(uint32_t ip)
{
    char buf[INET_ADDRSTRLEN];
    uint32_t net_ip = ntohl(ip);
    inet_ntop(AF_INET, &net_ip, buf, INET_ADDRSTRLEN);
    string str(buf);
    return str;
}

void parseConfigInit(struct ParseConfig* pconfig) {

}
