/**
 * @file pktFilterConfig.h Header file for packet filter configuration class.
 * @brief Config
 */
#ifndef __PKT_FILTER_CONFIG_H__
#define __PKT_FILTER_CONFIG_H__

#include "pcapHelper.h" // Packet_Elem

using namespace std;

enum Action {
	DROP, // blacklist
	KEEP  // whitelist
};

// Filter a specific type of traffic
class PktFilterConfig {
public:
	// default constructor, matches all packet
	PktFilterConfig()
	: tcp_flags_set(0), tcp_flags_clr(0), ip_proto(-1), \
		sport(-1), dport(-1), match_action(DROP) {}
	// Specify action
	PktFilterConfig(Action match_action) {
		init();
		this->match_action = match_action;
	}
	// Specified for tcp filtering
	PktFilterConfig(uint16_t tcp_flags_set, uint16_t tcp_flags_clr, \
		Action match_action) {
		init();
		this->tcp_flags_set = tcp_flags_set;
		this->tcp_flags_clr = tcp_flags_clr;
		this->match_action = match_action;
		// ip protocol should be tcp
		ip_proto = IP_PROTO_TCP;
	}
	// setters to set specific fields
	void set_sport(int num) {
		sport = num;
	}
	void set_dport(int num) {
		dport = num;
	}
	// whether the packet is dropped
	bool isDrop(Packet_Elem pkt);

private:
	uint16_t tcp_flags_set; // matched pkt must has these flags set
	uint16_t tcp_flags_clr; // matched pkt must has these flags cleared
	int ip_proto; // ip protocol, -1 if not check
	int sport; // source port
	int dport; // destination port
	Action match_action; // action if pkt matched
	// default init routine
	void init() {
		tcp_flags_set = 0;
		tcp_flags_clr = 0;
		ip_proto = -1;
		sport = -1;
		dport = -1;
		match_action = DROP;
	}
};

#endif /* !__PKT_FILTER_CONFIG_H__ */