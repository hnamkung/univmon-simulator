#include "netro_test_data_printout.h"

#ifdef __APPLE__
#define FMT_U64 "llx"
#else
#define FMT_U64 "lx"
#endif

void print_cw32(vector<countSketch> level_countSketch);
void make_output_CharTable32(vector<countSketch> level_countSketch);
void make_output_ShortTable32(vector<countSketch> level_countSketch);
void print_cw64(vector<countSketch> level_countSketch);


void print_params(vector<countSketch> level_countSketch);
void print_tabular2_params(vector<countSketch> level_countSketch);
void tabular2_make_output_file(vector<countSketch> level_countSketch);
void tabular5_make_output_file(vector<countSketch> level_countSketch);
void tabular5_reuse_make_output_file(vector<countSketch> level_countSketch);


void print_or_create_params(vector<countSketch> level_countSketch)
{
    const h_hash h;

    switch(h.global_hash_type)
    {
        case Alan_Hash:
            print_params(level_countSketch);
            break;

        case Shift_Hash:
            break;

        case Tabular2_Hash:
            // print_tabular2_params(level_countSketch);
            break;

        case Tabular5_Hash:
            //tabular5_make_output_file(level_countSketch);
            //tabular5_reuse_make_output_file(level_countSketch);
            break;        

        case CW32:
            print_cw32(level_countSketch);
            break;

        case CharTable32:
            make_output_CharTable32(level_countSketch);
            break;

        case ShortTable32:
            make_output_ShortTable32(level_countSketch);
            break;

        case CW64:
            print_cw64(level_countSketch);
            break;

        case CharTable64:
            break;
    }
    // tabular2_make_output_file(level_countSketch);
}

void print_cw32(vector<countSketch> level_countSketch)
{
    printf("__declspec(cls shared) uint64_t level_param[(SAMPLING_LEVEL+1) * 5] = {");
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        printf("0x%" FMT_U64 ", ", level_countSketch[i].sampling_hash.cw32_a);
        printf("0x%" FMT_U64 ", ", level_countSketch[i].sampling_hash.cw32_b);
        printf("0x%" FMT_U64 ", ", level_countSketch[i].sampling_hash.cw32_c);
        printf("0x%" FMT_U64 ", ", level_countSketch[i].sampling_hash.cw32_d);
        printf("0x%" FMT_U64 "", level_countSketch[i].sampling_hash.cw32_e);
        if(i != SAMPLING_LEVEL)
            printf(", ");
    }
    printf("};\n");

    printf("__declspec(cls shared) uint64_t index_param[ROW * 5] = {");
    for(int r=0; r<5; r++) {
        printf("0x%" FMT_U64 ", ", level_countSketch[0].index_hash[r].cw32_a);
        printf("0x%" FMT_U64 ", ", level_countSketch[0].index_hash[r].cw32_b);
        printf("0x%" FMT_U64 ", ", level_countSketch[0].index_hash[r].cw32_c);
        printf("0x%" FMT_U64 ", ", level_countSketch[0].index_hash[r].cw32_d);
        printf("0x%" FMT_U64 "", level_countSketch[0].index_hash[r].cw32_e);
        if(r != 4)
            printf(", ");
    }
    printf("};\n");

    printf("__declspec(cls shared) uint64_t res_param[ROW * 5] = {");
    for(int r=0; r<5; r++) {
        printf("0x%" FMT_U64 ", ", level_countSketch[0].res_hash[r].cw32_a);
        printf("0x%" FMT_U64 ", ", level_countSketch[0].res_hash[r].cw32_b);
        printf("0x%" FMT_U64 ", ", level_countSketch[0].res_hash[r].cw32_c);
        printf("0x%" FMT_U64 ", ", level_countSketch[0].res_hash[r].cw32_d);
        printf("0x%" FMT_U64 "", level_countSketch[0].res_hash[r].cw32_e);
        if(r != 4)
            printf(", ");
    }
    printf("};\n");
}

void print_cw64(vector<countSketch> level_countSketch)
{
    printf("__declspec(cls shared) uint64_t level_param[(SAMPLING_LEVEL+1) * 15] = {");
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_a0);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_a1);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_a2);

        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_b0);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_b1);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_b2);

        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_c0);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_c1);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_c2);

        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_d0);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_d1);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_d2);

        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_e0);
        printf("0x%x, ", level_countSketch[i].sampling_hash.cw64_e1);
        printf("0x%x", level_countSketch[i].sampling_hash.cw64_e2);
        if(i != SAMPLING_LEVEL)
            printf(", ");
    }
    printf("};\n");

    printf("__declspec(cls shared) uint64_t index_param[ROW * 15] = {");
    for(int r=0; r<5; r++) {
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_a0);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_a1);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_a2);

        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_b0);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_b1);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_b2);

        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_c0);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_c1);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_c2);

        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_d0);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_d1);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_d2);

        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_e0);
        printf("0x%x, ", level_countSketch[0].index_hash[r].cw64_e1);
        printf("0x%x", level_countSketch[0].index_hash[r].cw64_e2);
        if(r != 4)
            printf(", ");
    }
    printf("};\n");

    printf("__declspec(cls shared) uint64_t res_param[ROW * 15] = {");
    for(int r=0; r<5; r++) {
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_a0);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_a1);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_a2);

        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_b0);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_b1);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_b2);

        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_c0);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_c1);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_c2);

        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_d0);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_d1);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_d2);

        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_e0);
        printf("0x%x, ", level_countSketch[0].res_hash[r].cw64_e1);
        printf("0x%x", level_countSketch[0].res_hash[r].cw64_e2);
        if(r != 4)
            printf(", ");
    }
    printf("};\n");
}

void make_output_CharTable32(vector<countSketch> level_countSketch)
{
    mkdir("netro", 0777);
    mkdir("netro/output", 0777);
    mkdir("netro/output/char32", 0777);

    string path = "netro/output/char32/";
    
    string level_file_names[4] = {"level_t0", "level_t1", "level_t2", "level_t3"};
    for(int i=0; i<4; i++) {
        FILE *p = fopen((path+level_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint64_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            switch(i) {
                case 0:
                    array = level_countSketch[k].sampling_hash.char32_t0;
                    break;
                case 1:
                    array = level_countSketch[k].sampling_hash.char32_t1;
                    break;
                case 2:
                    array = level_countSketch[k].sampling_hash.char32_t2;
                    break;
                case 3:
                    array = level_countSketch[k].sampling_hash.char32_t3;
                    break;
            }

            for(int j=0; j<256; j+=2) {
                fprintf(p, "0x%010x: ", index);

               fprintf(p, "0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 "", array[j] >> 32, array[j] & 0xffffffff, array[j+1] >> 32, array[j+1] & 0xffffffff);
                // fprintf(p, "0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 "", array[j] & 0xffffffff, array[j] >> 32, array[j+1] & 0xffffffff, array[j+1] >> 32);

                fprintf(p, "\n");
                index+=16;
            }
        }
        fclose(p);
    }

    string level_file_names_2[3] = {"level_t4", "level_t5", "level_t6"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen((path+level_file_names_2[i]+".txt").c_str(), "w");
        int index = 0;

        uint16_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            int len = 0;
            switch(i) {
                case 0:
                    array = level_countSketch[k].sampling_hash.char32_t4;
                    len = 1024;
                    break;
                case 1:
                    array = level_countSketch[k].sampling_hash.char32_t5;
                    len = 1024;
                    break;
                case 2:
                    array = level_countSketch[k].sampling_hash.char32_t6;
                    len = 4096;
                    break;
            }

            for(int j=0; j<len; j+=8) {
                fprintf(p, "0x%010x: ", index);
                fprintf(p, "0x%04x%04x 0x%04x%04x 0x%04x%04x 0x%04x%04x", array[j], array[j+1], array[j+2], array[j+3], array[j+4], array[j+5], array[j+6], array[j+7]);
                fprintf(p, "\n");
                index+=16;
            }
        }
        fclose(p);
    }

    string index_file_names[4] = {"index_t0", "index_t1", "index_t2", "index_t3"};
    for(int i=0; i<4; i++) {
        FILE *p = fopen((path+index_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint64_t *array;
        for(int k=0; k<=0; k++) {
            for(int r=0; r<5; r++) {
                switch(i) {
                    case 0:
                        array = level_countSketch[k].index_hash[r].char32_t0;
                        break;
                    case 1:
                        array = level_countSketch[k].index_hash[r].char32_t1;
                        break;
                    case 2:
                        array = level_countSketch[k].index_hash[r].char32_t2;
                        break;
                    case 3:
                        array = level_countSketch[k].index_hash[r].char32_t3;
                        break;
                }

                for(int j=0; j<256; j+=2) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 "", array[j] >> 32, array[j] & 0xffffffff, array[j+1] >> 32, array[j+1] & 0xffffffff);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    string index_file_names_2[3] = {"index_t4", "index_t5", "index_t6"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen((path+index_file_names_2[i]+".txt").c_str(), "w");
        int index = 0;

        uint16_t *array;
        for(int k=0; k<=0; k++) {
            for(int r=0; r<5; r++) {
                int len = 0;
                switch(i) {
                    case 0:
                        array = level_countSketch[k].index_hash[r].char32_t4;
                        len = 1024;
                        break;
                    case 1:
                        array = level_countSketch[k].index_hash[r].char32_t5;
                        len = 1024;
                        break;
                    case 2:
                        array = level_countSketch[k].index_hash[r].char32_t6;
                        len = 4096;
                        break;
                }

                for(int j=0; j<len; j+=8) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%04x%04x 0x%04x%04x 0x%04x%04x 0x%04x%04x", array[j], array[j+1], array[j+2], array[j+3], array[j+4], array[j+5], array[j+6], array[j+7]);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    string res_file_names[4] = {"res_t0", "res_t1", "res_t2", "res_t3"};
    for(int i=0; i<4; i++) {
        FILE *p = fopen((path+res_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint64_t *array;
        for(int k=0; k<=0; k++) {
            for(int r=0; r<5; r++) {
                switch(i) {
                    case 0:
                        array = level_countSketch[k].res_hash[r].char32_t0;
                        break;
                    case 1:
                        array = level_countSketch[k].res_hash[r].char32_t1;
                        break;
                    case 2:
                        array = level_countSketch[k].res_hash[r].char32_t2;
                        break;
                    case 3:
                        array = level_countSketch[k].res_hash[r].char32_t3;
                        break;
                }

                for(int j=0; j<256; j+=2) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 "", array[j] >> 32, array[j] & 0xffffffff, array[j+1] >> 32, array[j+1] & 0xffffffff);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    string res_file_names_2[3] = {"res_t4", "res_t5", "res_t6"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen((path+res_file_names_2[i]+".txt").c_str(), "w");
        int index = 0;

        uint16_t *array;
        for(int k=0; k<=0; k++) {
            for(int r=0; r<5; r++) {
                int len = 0;
                switch(i) {
                    case 0:
                        array = level_countSketch[k].res_hash[r].char32_t4;
                        len = 1024;
                        break;
                    case 1:
                        array = level_countSketch[k].res_hash[r].char32_t5;
                        len = 1024;
                        break;
                    case 2:
                        array = level_countSketch[k].res_hash[r].char32_t6;
                        len = 4096;
                        break;
                }

                for(int j=0; j<len; j+=8) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%04x%04x 0x%04x%04x 0x%04x%04x 0x%04x%04x", array[j], array[j+1], array[j+2], array[j+3], array[j+4], array[j+5], array[j+6], array[j+7]);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    FILE *p = fopen((path+"sketchArray.txt").c_str(), "w");
    int sketch_array_size = (SAMPLING_LEVEL+1)*5*1024;
    int index=0;
    for(int j=0; j<sketch_array_size; j+=4) {
        fprintf(p, "0x%010x: 0x00000000 0x00000000 0x00000000 0x00000000\n", index);
        // for(int l=j; l<j+8; l+=2) {
        //     fprintf(p, "0x00000000 ");
        // }
        // fprintf(p, "\n");
        index+=16;
    }
    fclose(p);
}

void make_output_ShortTable32(vector<countSketch> level_countSketch)
{
    mkdir("netro", 0777);
    mkdir("netro/output", 0777);
    mkdir("netro/output/short32", 0777);
    string path = "netro/output/short32/";
    string level_file_names[3] = {"level_t0", "level_t1", "level_t2"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen((path+level_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint16_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            int len = 65536;
            switch(i) {
                case 0:
                    array = level_countSketch[k].sampling_hash.short32_t0;
                    break;
                case 1:
                    array = level_countSketch[k].sampling_hash.short32_t1;
                    break;
                case 2:
                    len = len*2;
                    array = level_countSketch[k].sampling_hash.short32_t2;
                    break;
            }

            for(int j=0; j<len; j+=8) {
                fprintf(p, "0x%010x: ", index);
                fprintf(p, "0x%04x%04x 0x%04x%04x 0x%04x%04x 0x%04x%04x", array[j], array[j+1], array[j+2], array[j+3], array[j+4], array[j+5], array[j+6], array[j+7]);
                fprintf(p, "\n");
                index+=16;
            }
        }
        fclose(p);
    }

    string index_file_names[3] = {"index_t0", "index_t1", "index_t2"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen((path+index_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint16_t *array;
        for(int k=0; k<=0; k++) {
            for(int r=0; r<5; r++) {
                int len = 65536;
                switch(i) {
                    case 0:
                        array = level_countSketch[k].index_hash[r].short32_t0;
                        break;
                    case 1:
                        array = level_countSketch[k].index_hash[r].short32_t1;
                        break;
                    case 2:
                        len = len*2;
                        array = level_countSketch[k].index_hash[r].short32_t2;
                        break;
                }

                for(int j=0; j<len; j+=8) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%04x%04x 0x%04x%04x 0x%04x%04x 0x%04x%04x", array[j], array[j+1], array[j+2], array[j+3], array[j+4], array[j+5], array[j+6], array[j+7]);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    string res_file_names[3] = {"res_t0", "res_t1", "res_t2"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen((path+res_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint16_t *array;
        for(int k=0; k<=0; k++) {
            for(int r=0; r<5; r++) {
                int len = 65536;
                switch(i) {
                    case 0:
                        array = level_countSketch[k].res_hash[r].short32_t0;
                        break;
                    case 1:
                        array = level_countSketch[k].res_hash[r].short32_t1;
                        break;
                    case 2:
                        len = len*2;
                        array = level_countSketch[k].res_hash[r].short32_t2;
                        break;
                }

                for(int j=0; j<len; j+=8) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%04x%04x 0x%04x%04x 0x%04x%04x 0x%04x%04x", array[j], array[j+1], array[j+2], array[j+3], array[j+4], array[j+5], array[j+6], array[j+7]);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    FILE *p = fopen((path+"sketchArray.txt").c_str(), "w");
    int sketch_array_size = (SAMPLING_LEVEL+1)*5*1024;
    int index=0;
    for(int j=0; j<sketch_array_size; j+=4) {
        fprintf(p, "0x%010x: 0x00000000 0x00000000 0x00000000 0x00000000\n", index);
        index+=16;
    }
    fclose(p);
}

void tabular5_make_output_file(vector<countSketch> level_countSketch)
{
    /*
    mkdir("netro", 0777);
    mkdir("netro/output", 0777);
    
    string level_file_names[4] = {"level_t5_0", "level_t5_1", "level_t5_2", "level_t5_3"};
    for(int i=0; i<4; i++) {
        FILE *p = fopen(("netro/output/"+level_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint64_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            switch(i) {
                case 0:
                    array = level_countSketch[k].sampling_hash.t5_0;
                    break;
                case 1:
                    array = level_countSketch[k].sampling_hash.t5_1;
                    break;
                case 2:
                    array = level_countSketch[k].sampling_hash.t5_2;
                    break;
                case 3:
                    array = level_countSketch[k].sampling_hash.t5_3;
                    break;
            }

            for(int j=0; j<256; j+=2) {
                fprintf(p, "0x%010x: ", index);

               fprintf(p, "0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 "", array[j] >> 32, array[j] & 0xffffffff, array[j+1] >> 32, array[j+1] & 0xffffffff);
                // fprintf(p, "0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 "", array[j] & 0xffffffff, array[j] >> 32, array[j+1] & 0xffffffff, array[j+1] >> 32);

                fprintf(p, "\n");
                index+=16;
            }
        }
        fclose(p);
    }

    string level_file_names_2[3] = {"level_t5_4", "level_t5_5", "level_t5_6"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen(("netro/output/"+level_file_names_2[i]+".txt").c_str(), "w");
        int index = 0;

        uint32_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            int len = 0;
            switch(i) {
                case 0:
                    array = level_countSketch[k].sampling_hash.t5_4;
                    len = 1024;
                    break;
                case 1:
                    array = level_countSketch[k].sampling_hash.t5_5;
                    len = 1024;
                    break;
                case 2:
                    array = level_countSketch[k].sampling_hash.t5_6;
                    len = 4096;
                    break;
            }

            for(int j=0; j<len; j+=4) {
                fprintf(p, "0x%010x: ", index);

                fprintf(p, "0x%08x 0x%08x 0x%08x 0x%08x", array[j], array[j+1], array[j+2], array[j+3]);

                fprintf(p, "\n");
                index+=16;
            }
        }
        fclose(p);
    }

    string index_file_names[4] = {"index_t5_0", "index_t5_1", "index_t5_2", "index_t5_3"};
    for(int i=0; i<4; i++) {
        FILE *p = fopen(("netro/output/"+index_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint64_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            for(int r=0; r<5; r++) {
                switch(i) {
                    case 0:
                        array = level_countSketch[k].index_hash[r].t5_0;
                        break;
                    case 1:
                        array = level_countSketch[k].index_hash[r].t5_1;
                        break;
                    case 2:
                        array = level_countSketch[k].index_hash[r].t5_2;
                        break;
                    case 3:
                        array = level_countSketch[k].index_hash[r].t5_3;
                        break;
                }

                for(int j=0; j<256; j+=2) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 "", array[j] >> 32, array[j] & 0xffffffff, array[j+1] >> 32, array[j+1] & 0xffffffff);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    string index_file_names_2[3] = {"index_t5_4", "index_t5_5", "index_t5_6"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen(("netro/output/"+index_file_names_2[i]+".txt").c_str(), "w");
        int index = 0;

        uint32_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            for(int r=0; r<5; r++) {
                int len = 0;
                switch(i) {
                    case 0:
                        array = level_countSketch[k].index_hash[r].t5_4;
                        len = 1024;
                        break;
                    case 1:
                        array = level_countSketch[k].index_hash[r].t5_5;
                        len = 1024;
                        break;
                    case 2:
                        array = level_countSketch[k].index_hash[r].t5_6;
                        len = 4096;
                        break;
                }

                for(int j=0; j<len; j+=4) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%08x 0x%08x 0x%08x 0x%08x", array[j], array[j+1], array[j+2], array[j+3]);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    string res_file_names[4] = {"res_t5_0", "res_t5_1", "res_t5_2", "res_t5_3"};
    for(int i=0; i<4; i++) {
        FILE *p = fopen(("netro/output/"+res_file_names[i]+".txt").c_str(), "w");
        int index = 0;

        uint64_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            for(int r=0; r<5; r++) {
                switch(i) {
                    case 0:
                        array = level_countSketch[k].res_hash[r].t5_0;
                        break;
                    case 1:
                        array = level_countSketch[k].res_hash[r].t5_1;
                        break;
                    case 2:
                        array = level_countSketch[k].res_hash[r].t5_2;
                        break;
                    case 3:
                        array = level_countSketch[k].res_hash[r].t5_3;
                        break;
                }

                for(int j=0; j<256; j+=2) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 " 0x%08" FMT_U64 "", array[j] >> 32, array[j] & 0xffffffff, array[j+1] >> 32, array[j+1] & 0xffffffff);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    string res_file_names_2[3] = {"res_t5_4", "res_t5_5", "res_t5_6"};
    for(int i=0; i<3; i++) {
        FILE *p = fopen(("netro/output/"+res_file_names_2[i]+".txt").c_str(), "w");
        int index = 0;

        uint32_t *array;
        for(int k=0; k<=SAMPLING_LEVEL; k++) {
            for(int r=0; r<5; r++) {
                int len = 0;
                switch(i) {
                    case 0:
                        array = level_countSketch[k].res_hash[r].t5_4;
                        len = 1024;
                        break;
                    case 1:
                        array = level_countSketch[k].res_hash[r].t5_5;
                        len = 1024;
                        break;
                    case 2:
                        array = level_countSketch[k].res_hash[r].t5_6;
                        len = 4096;
                        break;
                }

                for(int j=0; j<len; j+=4) {
                    fprintf(p, "0x%010x: ", index);
                    fprintf(p, "0x%08x 0x%08x 0x%08x 0x%08x", array[j], array[j+1], array[j+2], array[j+3]);
                    fprintf(p, "\n");
                    index+=16;
                }
            }
        }
        fclose(p);
    }

    FILE *p = fopen("netro/output/sketchArray.txt", "w");
    int sketch_array_size = (SAMPLING_LEVEL+1)*5*1024;
    int index=0;
    for(int j=0; j<sketch_array_size; j+=4) {
        fprintf(p, "0x%010x: 0x00000000 0x00000000 0x00000000 0x00000000\n", index);
        // for(int l=j; l<j+8; l+=2) {
        //     fprintf(p, "0x00000000 ");
        // }
        // fprintf(p, "\n");
        index+=16;
    }
    fclose(p);
    */
}


void print_params(vector<countSketch> level_countSketch)
{
    printf("__declspec(cls shared) uint64_t level_a[20] = {");
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        printf("%d", (int)level_countSketch[i].sampling_hash.aParams);
        if(i != SAMPLING_LEVEL)
            printf(", ");
    }
    printf("};\n");

    printf("__declspec(cls shared) uint64_t level_b[20] = {");
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        printf("%d", (int)level_countSketch[i].sampling_hash.bParams);
        if(i != SAMPLING_LEVEL)
            printf(", ");
    }
    printf("};\n");

    printf("__declspec(cls shared) uint64_t index_a[100] = {");
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int j=0; j<5; j++) {
            printf("%d", (int)level_countSketch[i].index_hash[j].aParams);
            if(i == SAMPLING_LEVEL && j == 4)
                printf("};\n");
            else
                printf(", ");
        }
    }

    printf("__declspec(cls shared) uint64_t index_b[100] = {");
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int j=0; j<5; j++) {
            printf("%d", (int)level_countSketch[i].index_hash[j].bParams);
            if(i == SAMPLING_LEVEL && j == 4)
                printf("};\n");
            else
                printf(", ");
        }
    }

    printf("__declspec(cls shared) uint64_t res_a[100] = {");
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int j=0; j<5; j++) {
            printf("%d", (int)level_countSketch[i].res_hash[j].aParams);
            if(i == SAMPLING_LEVEL && j == 4)
                printf("};\n");
            else
                printf(", ");
        }
    }

    printf("__declspec(cls shared) uint64_t res_b[100] = {");
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int j=0; j<5; j++) {
            printf("%d", (int)level_countSketch[i].res_hash[j].bParams);
            if(i == SAMPLING_LEVEL && j == 4)
                printf("};\n");
            else
                printf(", ");
        }
    }    
}

void print_tabular2_params(vector<countSketch> level_countSketch)
{
    int count = 0;
    int level_count = (SAMPLING_LEVEL+1) * 256;
    printf("__export __mem uint16_t level_t0[%d] = {", level_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int j=0; j<256; j++) {
            printf("%d", level_countSketch[i].sampling_hash.t0[j]);
            if(i == SAMPLING_LEVEL && j == 255) {
                printf("};\n");
            }
            else {
                printf(", ");
                count++;
                if(count % 50 == 0) {
                    count = 0;
                    printf("\n");
                }
            }
        }
    }

    printf("__export __mem uint16_t level_t1[%d] = {", level_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int j=0; j<256; j++) {
            printf("%d", level_countSketch[i].sampling_hash.t1[j]);
            if(i == SAMPLING_LEVEL && j == 255) {
                printf("};\n");
            }
            else {
                printf(", ");
                count++;
                if(count % 50 == 0) {
                    count = 0;
                    printf("\n");
                }
            }
        }
    }

    printf("__export __mem uint16_t level_t2[%d] = {", level_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int j=0; j<256; j++) {
            printf("%d", level_countSketch[i].sampling_hash.t2[j]);
            if(i == SAMPLING_LEVEL && j == 255) {
                printf("};\n");
            }
            else {
                printf(", ");
                count++;
                if(count % 50 == 0) {
                    count = 0;
                    printf("\n");
                }
            }
        }
    }

    printf("__export __mem uint16_t level_t3[%d] = {", level_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int j=0; j<256; j++) {
            printf("%d", level_countSketch[i].sampling_hash.t3[j]);
            if(i == SAMPLING_LEVEL && j == 255) {
                printf("};\n");
            }
            else {
                printf(", ");
                count++;
                if(count % 50 == 0) {
                    count = 0;
                    printf("\n");
                }
            }
        }
    }


    int index_count = (SAMPLING_LEVEL+1) * 5 * 256;
    printf("__export __mem uint16_t index_t0[%d] = {", index_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int k=0; k<5; k++) {
            for(int j=0; j<256; j++) {
                printf("%d", level_countSketch[i].index_hash[k].t0[j]);
                if(i == SAMPLING_LEVEL && k == 4 && j == 255) {
                    printf("};\n");
                }
                else {
                    printf(", ");
                    count++;
                    if(count % 50 == 0) {
                        count = 0;
                        printf("\n");
                    }
                }
            }
        }
    }

    printf("__export __mem uint16_t index_t1[%d] = {", index_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int k=0; k<5; k++) {
            for(int j=0; j<256; j++) {
                printf("%d", level_countSketch[i].index_hash[k].t1[j]);
                if(i == SAMPLING_LEVEL && k == 4 && j == 255) {
                    printf("};\n");
                }
                else {
                    printf(", ");
                    count++;
                    if(count % 50 == 0) {
                        count = 0;
                        printf("\n");
                    }
                }
            }
        }
    }

    printf("__export __mem uint16_t index_t2[%d] = {", index_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int k=0; k<5; k++) {
            for(int j=0; j<256; j++) {
                printf("%d", level_countSketch[i].index_hash[k].t2[j]);
                if(i == SAMPLING_LEVEL && k == 4 && j == 255) {
                    printf("};\n");
                }
                else {
                    printf(", ");
                    count++;
                    if(count % 50 == 0) {
                        count = 0;
                        printf("\n");
                    }
                }
            }
        }
    }

    printf("__export __mem uint16_t index_t3[%d] = {", index_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int k=0; k<5; k++) {
            for(int j=0; j<256; j++) {
                printf("%d", level_countSketch[i].index_hash[k].t3[j]);
                if(i == SAMPLING_LEVEL && k == 4 && j == 255) {
                    printf("};\n");
                }
                else {
                    printf(", ");
                    count++;
                    if(count % 50 == 0) {
                        count = 0;
                        printf("\n");
                    }
                }
            }
        }
    }

    int res_count = (SAMPLING_LEVEL+1) * 5 * 256;
    printf("__export __mem uint16_t res_t0[%d] = {", res_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int k=0; k<5; k++) {
            for(int j=0; j<256; j++) {
                printf("%d", level_countSketch[i].res_hash[k].t0[j]);
                if(i == SAMPLING_LEVEL && k == 4 && j == 255) {
                    printf("};\n");
                }
                else {
                    printf(", ");
                    count++;
                    if(count % 50 == 0) {
                        count = 0;
                        printf("\n");
                    }
                }
            }
        }
    }

    printf("__export __mem uint16_t res_t1[%d] = {", res_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int k=0; k<5; k++) {
            for(int j=0; j<256; j++) {
                printf("%d", level_countSketch[i].res_hash[k].t1[j]);
                if(i == SAMPLING_LEVEL && k == 4 && j == 255) {
                    printf("};\n");
                }
                else {
                    printf(", ");
                    count++;
                    if(count % 50 == 0) {
                        count = 0;
                        printf("\n");
                    }
                }
            }
        }
    }

    printf("__export __mem uint16_t res_t2[%d] = {", res_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int k=0; k<5; k++) {
            for(int j=0; j<256; j++) {
                printf("%d", level_countSketch[i].res_hash[k].t2[j]);
                if(i == SAMPLING_LEVEL && k == 4 && j == 255) {
                    printf("};\n");
                }
                else {
                    printf(", ");
                    count++;
                    if(count % 50 == 0) {
                        count = 0;
                        printf("\n");
                    }
                }
            }
        }
    }

    printf("__export __mem uint16_t res_t3[%d] = {", res_count);
    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        for(int k=0; k<5; k++) {
            for(int j=0; j<256; j++) {
                printf("%d", level_countSketch[i].res_hash[k].t3[j]);
                if(i == SAMPLING_LEVEL && k == 4 && j == 255) {
                    printf("};\n");
                }
                else {
                    printf(", ");
                    count++;
                    if(count % 50 == 0) {
                        count = 0;
                        printf("\n");
                    }
                }
            }
        }
    }
}