#ifndef __HH_H

#define __HH_H

#include <iostream>
#include <queue>
#include <vector>

#include <stdio.h>
#include <stdlib.h>
#include "stdint.h"

#define HEAVY_HITTER_K 100

using namespace std;

struct topk_elem {
	uint32_t feature;
	int estimate;
};

struct topk_elem_cmp{
    bool operator()(topk_elem a, topk_elem b){
        return a.estimate > b.estimate;
    }
};

class heavyHitters {

public:
	static const int topk_number = HEAVY_HITTER_K;

	heavyHitters();

	vector<topk_elem> topk;

	void topk_print();
	void topk_update(uint32_t feature, int estimate);
	void topk_update_change(uint32_t feature, int estimate);
	void sort();
    void reset();

private:
	bool full();
	int get_min();
	void insert(uint32_t feature, int estimate);
	void pop();
	void update_or_insert_or_replace(uint32_t feature, int estimate);
};

#endif
