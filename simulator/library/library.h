#ifndef __LIBRARY_H

#define __LIBRARY_H

#define SAMPLING_LEVEL 16

#include <string>
#include <map>

#include <netinet/in.h>

#include "countSketch.h"
#include "heavyHitters.h"
#include "pcapHelper.h"
#include "detectionHelper.h"
#include "resultHelper.h"
#include "pktFilterConfig.h"

using namespace std;

bool cmp(const topk_elem& first, const topk_elem& second);

void get_countsketch_and_heavyhitter(vector<Packet_Elem> &packets, vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters, int count_or_size);
void get_countsketch_and_heavyhitter64(vector<Packet_Elem> &packets, vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters, int count_or_size);
void get_countsketch_and_heavyhitter_change(vector<countSketch> &level_countSketch0, vector<Packet_Elem> &packets1, vector<countSketch> &level_countSketch1, vector<heavyHitters> &level_heavyHitters1, int count_or_size);
void get_countsketch_and_heavyhitter_latency(vector<Packet_Elem> &packets, vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters, int count_or_size, resultHelper &rh);
int get_gsum(vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters);
double get_entropy(vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters);
int get_gsum_customize(vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters, double(*g)(const int estimate));
heavyHitters get_countsketch_and_heavyhitter_unbalance(
    vector<countSketch> &level_countSketch0, vector<countSketch> &level_countSketch1,
    vector<heavyHitters> &level_heavyHitters0, vector<heavyHitters> &level_heavyHitters1,
    PktFilterConfig config0, PktFilterConfig config1,
    vector<Packet_Elem> &packets, int threshold, int count_or_size);

#endif
