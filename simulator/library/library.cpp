#include <iostream>
#include <vector>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <math.h>

#include "library.h"

#define TIME_INTERVAL   1000 //milliseconds
#define THRESHOLD (6.25 * 1024 * 1024) // 625KB/s 5Mbps
// #define THRESHOLD (200 * 1024) // 30KB


using namespace std;

bool cmp(const topk_elem& first, const topk_elem& second)
{
    if (first.estimate > second.estimate)
        return true;
    else
        return false;
}

int detect_ddos(heavyHitters &heavy_hitters)
{
    vector<topk_elem>::iterator it;
    it = heavy_hitters.topk.begin();
    for(; it != heavy_hitters.topk.end(); it ++) {
        uint32_t feature = it->feature;
        int estimate = it->estimate;
        if(estimate > THRESHOLD && feature == 185270273) {
            // cout << "[Univmon] DDoS detected: " << feature << ", " << estimate << endl;
            return estimate;
        }
    }
    return -1;
}

void reset_countsketch_and_heavyhitter(vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters)
{
    for(int i = 0; i <= SAMPLING_LEVEL; i ++) {
        level_countSketch[i].reset();
        level_heavyHitters[i].reset();
    }
}

void get_countsketch_and_heavyhitter(vector<Packet_Elem> &packets, vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters, int count_or_size)
{
    int hash;
    int elem;

    for(int pkt=0; pkt < packets.size(); pkt++) {
        uint32_t feature = packets[pkt].dst_ip;
        hash = 1;
        for (int i=0; i<=SAMPLING_LEVEL; i++) {
            if(i > 0) {
                hash *= level_countSketch[i].sampling_hash.select_sampling_hash(feature);
            }
            if(hash == 0)
                break;

            // printf("level %d\n", i);

            elem = 1;
            if(count_or_size == OPTION_SIZE) {
                elem = packets[pkt].size;
            }

            level_countSketch[i].sketch(feature, elem);
            int estimate = level_countSketch[i].estimate(feature);
            // printf("%20s (%12u) : +%d total %d\n", get_ip_string_from_int(feature).c_str(), feature, elem, estimate);
            // printf("\n\n");
            level_heavyHitters[i].topk_update(feature, estimate);
        }
    }

    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_heavyHitters[i].sort();
    }
}

void get_countsketch_and_heavyhitter64(vector<Packet_Elem> &packets, vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters, int count_or_size)
{
    int hash;
    int elem;

    for(int pkt=0; pkt < packets.size(); pkt++) {
        uint64_t feature = (((uint64_t)packets[pkt].src_ip) << 32) | (uint64_t)packets[pkt].dst_ip;
        hash = 1;
        for (int i=0; i<=SAMPLING_LEVEL; i++) {
            if(i > 0) {
                hash *= level_countSketch[i].sampling_hash.select_sampling_hash64(feature);
            }
            if(hash == 0)
                break;

            // printf("level %d\n", i);

            elem = 1;
            if(count_or_size == OPTION_SIZE) {
                elem = packets[pkt].size;
            }

            level_countSketch[i].sketch64(feature, elem);
            int estimate = level_countSketch[i].estimate64(feature);
            // printf("%20s (%12u) : +%d total %d\n", get_ip_string_from_int(feature).c_str(), feature, elem, estimate);
            // printf("\n\n");
            // level_heavyHitters[i].topk_update64(feature, estimate);
        }
    }

    // for(int i=0; i<=SAMPLING_LEVEL; i++) {
    //     level_heavyHitters[i].sort();
    // }
}

void get_countsketch_and_heavyhitter_change(vector<countSketch> &level_countSketch0, vector<Packet_Elem> &packets1, vector<countSketch> &level_countSketch1, vector<heavyHitters> &level_heavyHitters1, int count_or_size)
{
    int hash;
    int elem;

    for(int pkt=0; pkt < packets1.size(); pkt++) {
        uint32_t feature1 = packets1[pkt].dst_ip;
        hash = 1;
        for (int i=0; i<=SAMPLING_LEVEL; i++) {
            if(i > 0) {
                hash *= level_countSketch1[i].sampling_hash.hash(feature1, 2);
            }
            if(hash == 0)
                break;

            elem = 1;
            if(count_or_size == OPTION_SIZE) {
                elem = packets1[pkt].size;
            }

            level_countSketch1[i].sketch(feature1, elem);
            int estimate0 = level_countSketch0[i].estimate(feature1);
            int estimate1 = level_countSketch1[i].estimate(feature1);

            level_heavyHitters1[i].topk_update_change(feature1, abs(estimate1 - estimate0));
        }
    }

    for(int i=0; i<=SAMPLING_LEVEL; i++) {
        level_heavyHitters1[i].sort();
    }
}

void iterate_sketch_levels(vector<countSketch> &level_countSketch,
    vector<heavyHitters> &level_heavyHitters, Packet_Elem pkt,
    uint32_t feature, bool recordSize) {
    for (int l = 0, hash = 1; l <= SAMPLING_LEVEL; l++) {
        int elem = 1; // depends on whether size or count, record count by default
        int estimate = 0;
        if (l > 0) {
            hash *= level_countSketch[l].sampling_hash.hash(feature, 2);
        }
        if (hash == 0) {
            break;
        }
        if (recordSize) {
            elem = pkt.size;
        }
        if(level_countSketch[l].feature_type == FEATURE_DESTIP) {
            feature = pkt.dst_ip;
        } else if (level_countSketch[l].feature_type == FEATURE_SRCIP) {
            feature = pkt.src_ip;
        }
        // sketch
        level_countSketch[l].sketch(feature, elem);
        // estimate
        estimate = level_countSketch[l].estimate(feature);
        // update estimate top k
        level_heavyHitters[l].topk_update_change(feature, estimate);
    }
}

// check unblance between two types of traffic
// property of traffic is defined in class PktFilterConfig, check module for more details
heavyHitters get_countsketch_and_heavyhitter_unbalance(
    vector<countSketch> &level_countSketch0, vector<countSketch> &level_countSketch1,
    vector<heavyHitters> &level_heavyHitters0, vector<heavyHitters> &level_heavyHitters1,
    PktFilterConfig config0, PktFilterConfig config1,
    vector<Packet_Elem> &packets, int threshold, int count_or_size) {
    heavyHitters result = heavyHitters();

    for(int i = 0; i < packets.size(); i++) {
        uint32_t feature = packets[i].dst_ip;
        if (!config0.isDrop(packets[i])) {
            iterate_sketch_levels(level_countSketch0, level_heavyHitters0, packets[i], feature, count_or_size==count_or_size);
        }
        if (!config1.isDrop(packets[i])) {
            iterate_sketch_levels(level_countSketch1, level_heavyHitters1, packets[i], feature, count_or_size==count_or_size);
        }
    }

    // sort type 0 packet heavyhitters
    for (int l = 0; l <= SAMPLING_LEVEL; l++) {
        level_heavyHitters0[l].sort();
    }
    // check for unbalanced traffic
    for (vector<topk_elem>::iterator it = level_heavyHitters0[0].topk.begin();
        it != level_heavyHitters0[0].topk.end(); it++) {
        // estimate traffic in type 1 traffic
        int estimate = level_countSketch1[0].estimate(it->feature);
        int diff = abs(it->estimate - estimate);
        // report if exceed balance threshold
        if (diff > threshold) {
            result.topk_update(it->feature, diff);
            printf("feature: %d, estimate diff %d\n", it->feature, diff);
        }
    }
    return result;
}



void get_countsketch_and_heavyhitter_latency(vector<Packet_Elem> &packets,
                                            vector<countSketch> &level_countSketch,
                                            vector<heavyHitters> &level_heavyHitters,
                                            int count_or_size,
                                            resultHelper &rh)
{
    int hash;
    int elem;

    int detect_count = 0;

    // Get the start time
    uint64_t start_time = packets[0].timestamp;
    uint64_t prev_time = start_time;
    uint64_t cur_time = start_time;
    cout << "The start time is: " << start_time << endl;
    for(int pkt=0; pkt < packets.size(); pkt++) {
        cur_time = packets[pkt].timestamp;
        uint32_t feature = packets[pkt].dst_ip;
        hash = 1;
        for (int i=0; i<=SAMPLING_LEVEL; i++) {
            if(i > 0) {
                hash *= level_countSketch[i].sampling_hash.hash(feature, 2);
            }
            if(hash == 0)
                break;

            elem = 1;
            if(count_or_size == OPTION_SIZE) {
                elem = packets[pkt].size;
            }

            level_countSketch[i].sketch(feature, elem);
            int estimate = level_countSketch[i].estimate(feature);

            level_heavyHitters[i].topk_update(feature, estimate);

        }
        if(detect_ddos(level_heavyHitters[0]) > 0) {
            detect_count++;
            reset_countsketch_and_heavyhitter(level_countSketch, level_heavyHitters);
            // cout << prev_time << "--" << cur_time << ",";
            cout << " latency: " << cur_time - prev_time << "ms" << endl;
            rh.latency_result.back().um_latency_vector.push_back(cur_time - prev_time);
            if(detect_count >= 5)
                return;
            prev_time = cur_time;
        }
        if(cur_time - prev_time >= TIME_INTERVAL) {
            cout << "[Univmon] New second: " << prev_time << "--" << cur_time << endl;
            rh.latency_result.back().um_latency_vector.push_back(cur_time - prev_time);
            reset_countsketch_and_heavyhitter(level_countSketch, level_heavyHitters);
            prev_time = cur_time;
        }
    }

    //    for(int i=0; i<=SAMPLING_LEVEL; i++) {
    //        level_heavyHitters[i].sort();
    //    }
}

int get_gsum(vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters)
{
    double yBottom=0.0, y;

    int k = SAMPLING_LEVEL;
    vector<topk_elem>::iterator it;

    for (it = level_heavyHitters[k].topk.begin(); it != level_heavyHitters[k].topk.end(); it++) {
        yBottom += it->estimate;
    }

    for(k=SAMPLING_LEVEL-1; k>=0; k--) {
        y = 0;
        for (it = level_heavyHitters[k].topk.begin(); it != level_heavyHitters[k].topk.end(); it++) {
            uint32_t src = it->feature;
            int estimate = it->estimate;

            double unit = (1.0 - 2.0 * level_countSketch[k+1].sampling_hash.select_sampling_hash(src));
            y += (unit * estimate);
        }
        yBottom = 2 * yBottom + y;
    }
    return yBottom;
}

double xlogx(int estimate)
{
    return (double)estimate * log2((double)estimate);
}

double get_entropy(vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters)
{
    double yBottom=0.0, y;

    int k = SAMPLING_LEVEL;
    vector<topk_elem>::iterator it;

    for (it = level_heavyHitters[k].topk.begin(); it != level_heavyHitters[k].topk.end(); it++) {
        yBottom += xlogx(it->estimate);
    }

    for(k=SAMPLING_LEVEL-1; k>=0; k--) {
        y = 0;
        for (it = level_heavyHitters[k].topk.begin(); it != level_heavyHitters[k].topk.end(); it++) {
            uint32_t src = it->feature;
            int estimate = xlogx(it->estimate);

            double unit = (1.0 - 2.0 * level_countSketch[k+1].sampling_hash.select_sampling_hash(src));
            y += (unit * estimate);
        }
        yBottom = 2 * yBottom + y;
    }
    return yBottom;
}

int get_gsum_customize(vector<countSketch> &level_countSketch, vector<heavyHitters> &level_heavyHitters, double(*g)(const int estimate))
{
    double yBottom=0.0, y;

    int k = SAMPLING_LEVEL;
    vector<topk_elem>::iterator it;

    for (it = level_heavyHitters[k].topk.begin(); it != level_heavyHitters[k].topk.end(); it++) {
        // gsum here
        yBottom += g(it->estimate);
    }

    for(k=SAMPLING_LEVEL-1; k>=0; k--) {
        y = 0;
        for (it = level_heavyHitters[k].topk.begin(); it != level_heavyHitters[k].topk.end(); it++) {
            // gsum here
            uint32_t src = it->feature;
            double estimate = g(it->estimate);

            double unit = (1.0 - 2.0 * level_countSketch[k+1].sampling_hash.select_sampling_hash(src));
            y += (unit * estimate);
        }
        yBottom = 2 * yBottom + y;
    }
    return yBottom;
}


