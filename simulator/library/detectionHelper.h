#ifndef __ATTACKH_H

#define __ATTACKH_H

#include <algorithm>
#include <vector>
#include <map>

#include "pcapHelper.h"
#include "library.h"

using namespace std;

class detectionHelper {

public:
	detectionHelper()
	{
        for(int i=0; i<=SAMPLING_LEVEL; i++) {
            level_countSketch.push_back(countSketch());
        }
	}

    void reset()
    {
        packets_gt.clear();
        packetMap_gt.clear();
        packets_nf.clear();
        packetMap_nf.clear();

        packets0_gt.clear();
        packetMap0_gt.clear();
        packets1_gt.clear();
        packetMap1_gt.clear();

        packets0_nf.clear();
        packetMap0_nf.clear();
        packets1_nf.clear();
        packetMap1_nf.clear();
    }

    char* get_file_name()
    {
        return pcap_file_name;
    }

    char* get_file_name_0()
    {
        return pcap_file_name_0;
    }

    char* get_file_name_1()
    {
        return pcap_file_name_1;
    }

    char* pcap_file_name;
    char* pcap_file_name_0;
    char* pcap_file_name_1;

    // data structure for ground truth
	vector<Packet_Elem> packets_gt;
	map <uint32_t, int> packetMap_gt;

    // data structure for net flow
	vector<Packet_Elem> packets_nf;
	map <uint32_t, int> packetMap_nf;

    // for change detection 
    vector<Packet_Elem> packets0_gt;
    map <uint32_t, int> packetMap0_gt;
    vector<Packet_Elem> packets1_gt;
    map <uint32_t, int> packetMap1_gt;

    vector<Packet_Elem> packets0_nf;
    map <uint32_t, int> packetMap0_nf;
    vector<Packet_Elem> packets1_nf;
    map <uint32_t, int> packetMap1_nf;

    vector<countSketch> level_countSketch;


};

#endif
