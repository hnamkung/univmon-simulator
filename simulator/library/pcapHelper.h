#ifndef __PCAPH_H

#define __PCAPH_H

#include <string>
#include <map>

#include <netinet/in.h>

#include "countSketch.h"
#include "heavyHitters.h"

#define OPTION_COUNT 0
#define OPTION_SIZE 1

// commonly used protocol numbers
#define IP_PROTO_ICMP  0x01
#define IP_PROTO_IGMP  0x02
#define IP_PROTO_TCP   0x06
#define IP_PROTO_UDP   0x11

// TCP flags
#define TCP_FLAG_URG (1 << 10)
#define TCP_FLAG_ACK (1 << 11)
#define TCP_FLAG_PSH (1 << 12)
#define TCP_FLAG_RST (1 << 13)
#define TCP_FLAG_SYN (1 << 14)
#define TCP_FLAG_FIN (1 << 15)

// set tcp flags
#define SET_TCP_FLAGS(toset, flags) (toset |= flags)
#define CLR_TCP_FLAGS(toclr, flags) (toclr &= ~flags)
#define CHK_TCP_FALGS_SET(tochk, flags)     ( ((tochk) & (flags)) == (flags) )
#define CHK_TCP_FLAGS_HASSET(tochk, flags)  ( ((tochk) & (flags)) != 0 )

using namespace std;

struct Packet_Elem {
	uint32_t src_ip;
	char src_ip_addr[INET_ADDRSTRLEN];

	uint32_t dst_ip;
	char dst_ip_addr[INET_ADDRSTRLEN];

	uint16_t src_port;
	uint16_t dst_port;

	// tcp flags
	uint16_t tcp_flags;

	uint16_t size;

    uint64_t timestamp; //in millisecond

    // protocols
    int ip_proto;
};

enum NetflowType
{
    Uniform_Sampling,
    Random_Sampling,
    Sample_and_Hold
};


class NetflowConfig {
public:
	NetflowConfig() {
		type = Uniform_Sampling;
		sampling_count = 1000;
	}

	NetflowConfig(NetflowType _type, int _sampling_count) {
		type = _type;
		sampling_count = _sampling_count;
	}

	NetflowType type;
	int sampling_count;
};

class ParseConfig {
public:
	ParseConfig() {
		flag_set = 0;
		flag_not = 0;
		proto = -1;
		is_dest = true;
	}
	ParseConfig(uint16_t flag_set, uint16_t flag_not, int proto, bool is_dest) {
		this->flag_set = flag_set;
		this->flag_not = flag_not;
		this->proto = proto;
		this->is_dest = is_dest;
	}
	uint16_t get_flag_set() {
		return flag_set;
	}
	uint16_t get_flag_not() {
		return flag_not;
	}
	int get_proto() {
		return proto;
	}
	bool get_is_dest() {
		return is_dest;
	}

private:
	uint16_t flag_set;
	uint16_t flag_not;
	int proto;
	bool is_dest; // store dest or not
};

void pcapParse(string pcapFile, vector<Packet_Elem> &packets, int _sampling_rate, map<uint32_t, int> &packetMap, int _count_or_size, ParseConfig pconfig);
void pcapParse_netflow(string pcapFile, vector<Packet_Elem> &packets, map<uint32_t, int> &packetMap, int _count_or_size, NetflowConfig nf_config);
void pcap_get_info(char* input_file_name, int &pcap_packet_count, int &pcap_byte_size);
void get_ip_char_from_int(char* ip_addr, uint32_t ip);
string get_ip_string_from_int(uint32_t ip);

#endif
