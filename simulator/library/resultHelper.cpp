#include <iostream>
#include <list>
#include <algorithm>

#include <stdio.h>
#include <stdlib.h>

#include "resultHelper.h"
#include "library.h"
#include "heavyHitters.h"
#include "pcapHelper.h"

using namespace std;

void resultHelper::reset()
{
    gt_result.clear();
    nf_result.clear();
    um_result.clear();
    um64_result.clear();
}

void resultHelper::print_detection_result(map<uint32_t, int> result)
{
    list<topk_elem> sorted_list;

	map <uint32_t, int>::iterator it;
    for (it = result.begin(); it != result.end(); ++it) {
	    struct topk_elem lm;
	    lm.feature = it->first;
	    lm.estimate = it->second;
	    sorted_list.push_back(lm);
    }

    int c=0;
    sorted_list.sort(cmp);
    list<topk_elem>::iterator sit;
    for (sit = sorted_list.begin(); sit != sorted_list.end(); sit++){
        c++;
        printf("%3d] %20s (%12u) : %d\n", c, get_ip_string_from_int(sit->feature).c_str(), sit->feature, sit->estimate);
    }
}

void resultHelper::print_detection_result64(map<uint64_t, int> result)
{
    map <uint64_t, int>::iterator it;
    for (it = result.begin(); it != result.end(); ++it) {
        uint32_t src = (it->first) >> 32;
        uint32_t dst = (it->first) & 0xffffffff;
        int estimate = it->second;
        printf("%20s (%12u) <-> %20s (%12u) : %d\n", get_ip_string_from_int(src).c_str(), src, get_ip_string_from_int(dst).c_str(), dst, estimate);
    }
}

void resultHelper::gt_print()
{
    printf("------------ground truth------------\n");
	print_detection_result(gt_result);
    printf("\n\n");
}

void resultHelper::nf_print()
{
    printf("------------net flow------------\n");
	print_detection_result(nf_result);
    printf("\n\n");
}

void resultHelper::um_print()
{
    printf("------------univ mon------------\n");
	print_detection_result(um_result);
    printf("\n\n");
}

void resultHelper::um64_print()
{
    printf("------------univ mon 64------------\n");
    print_detection_result64(um64_result);
    printf("\n\n");
}

void resultHelper::print_result()
{
    gt_print();
    nf_print();
    um_print();
    um64_print();
}

void resultHelper::push_back_latency_result(char* pcap_file_name)
{
    struct Latency_Elem lm;
    string str(pcap_file_name);
    lm.pcap_name = str;
    latency_result.push_back(lm);    
}

void resultHelper::print_latency()
{
    for(int i=0; i<latency_result.size(); i++) {
        cout << latency_result[i].pcap_name << " ";
        sort (latency_result[i].nf_latency_vector.begin(), latency_result[i].nf_latency_vector.end());
        sort (latency_result[i].um_latency_vector.begin(), latency_result[i].um_latency_vector.end());

        int size = latency_result[i].nf_latency_vector.size();
        cout << latency_result[i].nf_latency_vector[0] << " ";
        cout << latency_result[i].nf_latency_vector[size*0.25] << " ";
        cout << latency_result[i].nf_latency_vector[size*0.5] << " ";
        cout << latency_result[i].nf_latency_vector[size*0.75] << " ";
        cout << latency_result[i].nf_latency_vector[size-1] << " ";
        for(int j=0; j<5; j++) {
            cout << latency_result[i].um_latency_vector[j] << " ";
        }

        cout << endl;
    }
}








