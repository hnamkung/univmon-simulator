#ifndef __h_hash_H

#define __h_hash_H

#include "stdint.h"

enum HashType
{
    Alan_Hash,
    Shift_Hash,
    Tabular2_Hash,
    Tabular5_Hash,

    CW32,
    CharTable32,
    ShortTable32,

    CW64,
    CharTable64
//    ShortTable64
};

class h_hash {

public:

	typedef struct
	{
		uint64_t h;
		uint64_t u;
		uint32_t v;
	} Entry;

	static HashType global_hash_type;

	static const long long int primeNumber = 39916801;
	long long int aParams;
	long long int bParams;

	// for 2-independent
	uint16_t t0[256];
	uint16_t t1[256];
	uint16_t t2[256];
	uint16_t t3[256];

	// for 5 independent
	uint64_t t5_0[256];
	uint64_t t5_1[256];
	uint64_t t5_2[256];
	uint64_t t5_3[256];

	uint16_t t5_4[1024];
	uint16_t t5_5[1024];

	uint16_t t5_6[4096];

	// 1. CW32
	static const uint64_t cw32_p = (((uint64_t)1) << 61) - 1;
	uint64_t cw32_a, cw32_b, cw32_c, cw32_d, cw32_e;

	// 2. CharTable32
	uint64_t char32_t0[256];
	uint64_t char32_t1[256];
	uint64_t char32_t2[256];
	uint64_t char32_t3[256];

	uint16_t char32_t4[1024];
	uint16_t char32_t5[1024];

	uint16_t char32_t6[4096];

	// 3. ShortTable32
	uint16_t short32_t0[65536];
	uint16_t short32_t1[65536];
	uint16_t short32_t2[65536*2];

	// 4. CW64
	static const uint64_t cw64_p0 = (((uint64_t)1) << 32) - 1;
	static const uint64_t cw64_p1 = (((uint64_t)1) << 32) - 1;
	static const uint64_t cw64_p2 = (((uint64_t)1) << 25) - 1;
	static const uint64_t cw64_p3 = (((uint64_t)1) << 57) - 1;
	uint32_t cw64_r0, cw64_r1, cw64_r2;
	uint32_t cw64_a0, cw64_a1, cw64_a2;
	uint32_t cw64_b0, cw64_b1, cw64_b2;
	uint32_t cw64_c0, cw64_c1, cw64_c2;
	uint32_t cw64_d0, cw64_d1, cw64_d2;
	uint32_t cw64_e0, cw64_e1, cw64_e2;

	// 5. CharTable64
	// Entry char64_t0[65536];
	// Entry char64_t1[65536];
	// Entry char64_t2[65536];
	// Entry char64_t3[65536];
	// Entry char64_t4[65536];
	// Entry char64_t5[65536];
	// Entry char64_t6[65536];
	// Entry char64_t7[65536];
	// uint16_t char64_t8[2048];
	// uint16_t char64_t9[2048];
	// uint16_t char64_t10[2048];
	// uint16_t char64_t11[2048];
	// uint16_t char64_t12[2097152];
	// uint16_t char64_t13[2048];
	// uint16_t char64_t14[4194304];

	h_hash();

	uint64_t rand64();
	uint64_t rand32();
	uint16_t rand16();
	int select_sampling_hash(uint32_t feature);
	int select_sampling_hash64(uint64_t feature);

	int select_index_hash(uint32_t feature);
	int select_index_hash64(uint64_t feature);

	int select_res_hash(uint32_t feature);
	int select_res_hash64(uint64_t feature);

    int hash(uint32_t x, int range);
	int shift_hash(uint32_t x, int bit);
	int tabulation2_hash(uint32_t x);
	int tabulation5_hash(uint32_t x);
	
	uint64_t MultAddPrime32(uint32_t x, uint64_t a, uint64_t b);
	int cw32_hash(uint32_t x);
	int char32_hash(uint32_t x);
	int short32_hash(uint32_t x);

	// cw64
	uint64_t Mod64Prime89();
	void MultAddPrime89(uint64_t x, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t b0, uint32_t b1, uint32_t b2);
	int cw64_hash(uint64_t x);
	int char64_hash(uint64_t x);

private:

};

#endif
