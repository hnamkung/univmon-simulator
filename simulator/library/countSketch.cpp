#include <iostream>
#include <algorithm>

#include "library.h"
#include "countSketch.h"
#include "h_hash.h"

using namespace std;

countSketch::countSketch()
{
    sketchArray = new int[row * width];
    for(int i=0; i<row * width; i++)
        sketchArray[i] = 0;

    // memset(sketchArray, 0, row * width);

    sampling_hash = h_hash();
    for(int i=0; i<row; i++) {
        index_hash.push_back(h_hash());
        res_hash.push_back(h_hash());
    }
    this->feature_type = FEATURE_DESTIP;
}

countSketch::countSketch(int level)
{
    sketchArray = new int[row * width];
    for(int i=0; i<row * width; i++)
        sketchArray[i] = 0;

    // memset(sketchArray, 0, row * width);

    sampling_hash = h_hash();
    if(level == 0) {
        for(int i=0; i<row; i++) {
            index_hash.push_back(h_hash());
            res_hash.push_back(h_hash());
        }
    }
    this->feature_type = FEATURE_DESTIP;
}

countSketch::countSketch(FeatureType type)
{
    sketchArray = new int[row * width];
    for(int i=0; i<row * width; i++)
        sketchArray[i] = 0;

    // memset(sketchArray, 0, row * width);

    sampling_hash = h_hash();
    for(int i=0; i<row; i++) {
        index_hash.push_back(h_hash());
        res_hash.push_back(h_hash());
    }
    this->feature_type = type;
}

void countSketch::sketch_print()
{
	for(int i=0; i<row * width; i++) {
		printf("0x%x\n", sketchArray[i]);
//		printf(" '0x%08x',\n", sketchArray[i]);
	}
}

void countSketch::sketch(uint32_t feature, int elem)
{
    // printf("feature : %12u\n", feature);
	for(int i=0; i<row; i++) {
        // printf("row : %d \n", i);
        int index = index_hash[i].select_index_hash(feature);
        // printf("index[%d]\n", index);
        int res = res_hash[i].select_res_hash(feature);
        // printf("res[%d]\n", res);
        // printf("\n");

		res = res*2 - 1;
		sketchArray[i*width + index] += (res * elem);
	}
}

void countSketch::sketch64(uint64_t feature, int elem)
{
    // printf("feature : %12u\n", feature);
    for(int i=0; i<row; i++) {
        // printf("row : %d \n", i);
        int index = index_hash[i].select_index_hash64(feature);
        // printf("index[%d]\n", index);
        int res = res_hash[i].select_res_hash64(feature);
        // printf("res[%d]\n", res);
        // printf("\n");

        res = res*2 - 1;
        sketchArray[i*width + index] += (res * elem);
    }
}

int countSketch::estimate(uint32_t feature)
{
    int a[row];
    for (int i=0; i<row; i++) {
        int index = index_hash[i].select_index_hash(feature);
        int res = res_hash[i].select_res_hash(feature);
        
		res = res*2 - 1;
        a[i]=sketchArray[i*width + index] * res;
    }
    sort(a, a + row);

    if (row %2 ==0) {
        return (a[row/2]+a[row/2-1])/2;
    } else {
        return a[(row/2)];
    }
}

int countSketch::estimate64(uint64_t feature)
{
    int a[row];
    for (int i=0; i<row; i++) {
        int index = index_hash[i].select_index_hash64(feature);
        int res = res_hash[i].select_res_hash64(feature);
        
        res = res*2 - 1;
        a[i]=sketchArray[i*width + index] * res;
    }
    sort(a, a + row);

    if (row %2 ==0) {
        return (a[row/2]+a[row/2-1])/2;
    } else {
        return a[(row/2)];
    }
}

void countSketch::reset()
{
	for(int i=0; i<row * width; i++)
		sketchArray[i] = 0;
}

void countSketch::set_feature_type(FeatureType type)
{
    this->feature_type = type;
}
