#include <iostream>
#include <ctime>
#include <cstdlib>

#include <time.h>
#include <stdio.h>

#include "h_hash.h"
#include "countSketch.h"

using namespace std;

// default
HashType h_hash::global_hash_type = Alan_Hash;

uint64_t h_hash::rand64()
{
	uint64_t r;
	r = rand();
	r = r << 32 | rand();
	return r;
}

uint64_t h_hash::rand32()
{
	uint32_t r;
	r = rand();
	return r;
}

uint16_t h_hash::rand16()
{
	uint16_t r;
	r = rand() & 65535;
	return r;
}

h_hash::h_hash()
{
//	aParams = rand() % primeNumber;
//	bParams = rand() % primeNumber;
	aParams = rand();
	aParams = aParams << 32 | rand();
	bParams = rand();
	bParams = bParams << 32 | rand();

	int i=0;
	for(i=0; i<256; i++) {
		
		t0[i] = rand() & 1023;
		t1[i] = rand() & 1023;
		t2[i] = rand() & 1023;
		t3[i] = rand() & 1023;

		t5_0[i] = rand();
		t5_0[i] = t5_0[i] << 32 | rand();

		t5_1[i] = rand();
		t5_1[i] = t5_1[i] << 32 | rand();

		t5_2[i] = rand();
		t5_2[i] = t5_2[i] << 32 | rand();

		t5_3[i] = rand();
		t5_3[i] = t5_3[i] << 32 | rand();

	}
	for(i=0; i<1024; i++) {
		t5_4[i] = rand() & 65535;
		t5_5[i] = rand() & 65535;
	}

	for(i=0; i<4096; i++) {
		t5_6[i] = rand() & 65535;
	}


	// 1. CW32
	cw32_a = rand64();
	cw32_b = rand64();
	cw32_c = rand64();
	cw32_d = rand64();
	cw32_e = rand64();


	// 2. CharTable32
	for(i=0; i<256; i++) {
		char32_t0[i] = rand64();
		char32_t1[i] = rand64();
		char32_t2[i] = rand64();
		char32_t3[i] = rand64();
	}

	for(i=0; i<1024; i++) {
		char32_t4[i] = rand16();
		char32_t5[i] = rand16();
	}

	for(i=0; i<4096; i++) {
		char32_t6[i] = rand16();
	}

	// 3. ShortTable32
	for(i=0; i<65536; i++) {
		short32_t0[i] = rand16();
		short32_t1[i] = rand16();
	}
	for(i=0; i<65536*2; i++) {
		short32_t2[i] = rand16();
	}

	// 4. CW64
	cw64_a0 = rand();
	cw64_a1 = rand();
	cw64_a2 = rand();

	cw64_b0 = rand();
	cw64_b1 = rand();
	cw64_b2 = rand();

	cw64_c0 = rand();
	cw64_c1 = rand();
	cw64_c2 = rand();

	cw64_d0 = rand();
	cw64_d1 = rand();
	cw64_d2 = rand();

	cw64_e0 = rand();
	cw64_e1 = rand();
	cw64_e2 = rand();

	/*
	// 5. CharTable64
	for(i=0; i<65536; i++) {
		char64_t0[i].h = rand64();
		char64_t0[i].u = rand64();
		char64_t0[i].v = rand32();

		char64_t1[i].h = rand64();
		char64_t1[i].u = rand64();
		char64_t1[i].v = rand32();

		char64_t2[i].h = rand64();
		char64_t2[i].u = rand64();
		char64_t2[i].v = rand32();

		char64_t3[i].h = rand64();
		char64_t3[i].u = rand64();
		char64_t3[i].v = rand32();

		char64_t4[i].h = rand64();
		char64_t4[i].u = rand64();
		char64_t4[i].v = rand32();

		char64_t5[i].h = rand64();
		char64_t5[i].u = rand64();
		char64_t5[i].v = rand32();

		char64_t6[i].h = rand64();
		char64_t6[i].u = rand64();
		char64_t6[i].v = rand32();

		char64_t7[i].h = rand64();
		char64_t7[i].u = rand64();
		char64_t7[i].v = rand32();
	}

	for(i=0; i<2048; i++) {
		char64_t8[i] = rand16();
		char64_t9[i] = rand16();
		char64_t10[i] = rand16();
		char64_t11[i] = rand16();
		char64_t13[i] = rand16();
	}

	for(i=0; i<2097152; i++) {
		char64_t12[i] = rand16();
	}

	for(i=0; i<4194304; i++) {
		char64_t14[i] = rand16();
	}
	*/
}

int h_hash::select_sampling_hash(uint32_t feature)
{
    switch(global_hash_type)
    {
        case Alan_Hash:
            return hash(feature, 2);
        case Shift_Hash:
            return shift_hash(feature, 0x1);
        case Tabular2_Hash:
            return tabulation2_hash(feature) & 1;
        case Tabular5_Hash:
            return tabulation5_hash(feature) & 1;

    	case CW32:
    		return cw32_hash(feature) & 1;
    	case CharTable32:
    		return char32_hash(feature) & 1;
    	case ShortTable32:
    		return short32_hash(feature) & 1;
    }
    printf("sampling hash error!\n");
    return -1;
}

int h_hash::select_sampling_hash64(uint64_t feature)
{
    switch(global_hash_type)
    {
    	case CW64:
    		return cw64_hash(feature) & 1;
	    case CharTable64:
    		return char64_hash(feature) & 1;

    }
    printf("sampling hash error!\n");
    return -1;

}

int h_hash::select_index_hash(uint32_t feature)
{
    switch(global_hash_type)
    {
        case Alan_Hash:
            return hash(feature, COUNT_SKETCH_WIDTH);
        case Shift_Hash:
            return shift_hash(feature, 1023);
        case Tabular2_Hash:
            return tabulation2_hash(feature) & 1023;
        case Tabular5_Hash:
            return tabulation5_hash(feature) & 1023;
    	
    	case CW32:
    		return cw32_hash(feature) & 1023;
    	case CharTable32:
    		return char32_hash(feature) & 1023;
    	case ShortTable32:
    		return short32_hash(feature) & 1023;
    }
    printf("index hash error!\n");
    return -1;
}

int h_hash::select_index_hash64(uint64_t feature)
{
    switch(global_hash_type)
    {
    	case CW64:
    		return cw64_hash(feature) & 1023;
	    case CharTable64:
    		return char64_hash(feature) & 1023;
    }
    printf("index hash error!\n");
    return -1;

}

int h_hash::select_res_hash(uint32_t feature)
{
    switch(global_hash_type)
    {
        case Alan_Hash:
            return hash(feature, 2);
        case Shift_Hash:
            return shift_hash(feature, 0x1);
        case Tabular2_Hash:
            return tabulation2_hash(feature) & 1;
        case Tabular5_Hash:
            return tabulation5_hash(feature) & 1;
    	
    	case CW32:
    		return cw32_hash(feature) & 1;
    	case CharTable32:
    		return char32_hash(feature) & 1;
    	case ShortTable32:
    		return short32_hash(feature) & 1;
    }
    printf("res hash error!\n");
    return -1;
}

int h_hash::select_res_hash64(uint64_t feature)
{
    switch(global_hash_type)
    {
    	case CW64:
    		return cw64_hash(feature) & 1;
	    case CharTable64:
    		return char64_hash(feature) & 1;
    }
    printf("res hash error!\n");
    return -1;	
}

int h_hash::hash(uint32_t x, int range)
{	
	// return ((aParams * (long long int)x + bParams) % primeNumber) % range;
	// for netronome test
	return (((int)aParams * x + (int)bParams) % primeNumber) % range;
}

int h_hash::shift_hash(uint32_t x, int mask)
{
	return ((aParams * (long long int)x + bParams) >> 32) & mask;
}

int h_hash::tabulation2_hash(uint32_t x)
{
	uint8_t x0, x1, x2, x3;

	x0 = (x >> 24) & 0xff;
	x1 = (x >> 16) & 0xff;
	x2 = (x >> 8) & 0xff;
	x3 = x & 0xff;

	return t0[x0] ^ t1[x1] ^ t2[x2] ^ t3[x3];
}

int h_hash::tabulation5_hash(uint32_t x)
{
	uint8_t x0, x1, x2, x3;
	uint32_t a00, a01, a10, a11, a20, a21, a30, a31;
	uint32_t c;

	x0 = (x >> 24) & 0xff;
	x1 = (x >> 16) & 0xff;
	x2 = (x >> 8) & 0xff;
	x3 = x & 0xff;

	a00 = (t5_0[x0] >> 32) & 0xffffffff;
	a01 = t5_0[x0] & 0xffffffff;

	a10 = (t5_1[x1] >> 32) & 0xffffffff;
	a11 = t5_1[x1] & 0xffffffff;

	a20 = (t5_2[x2] >> 32) & 0xffffffff;
	a21 = t5_2[x2] & 0xffffffff;

	a30 = (t5_3[x3] >> 32) & 0xffffffff;
	a31 = t5_3[x3] & 0xffffffff;


	c = a01 + a11 + a21 + a31;
	return a00 ^ a10 ^ a20 ^ a30 ^ t5_4[c&1023] ^ t5_5[(c>>10)&1023] ^ t5_6[c>>20];
}

/*
	printf("x0 = 0x%08x\n", x0);
	printf("a00 = 0x%08x\n", a00);
	printf("a01 = 0x%08x\n", a01);
	printf("a10 = 0x%08x\n", a10);
	printf("a11 = 0x%08x\n", a11);
	printf("a20 = 0x%08x\n", a20);
	printf("a21 = 0x%08x\n", a21);
	printf("a30 = 0x%08x\n", a30);
	printf("a31 = 0x%08x\n", a31);

	printf("c = 0x%08x\n", c);

	printf("c&1023 = 0x%08x\n", c&1023);
	printf("(c>>10)&1023 = 0x%08x\n", (c>>10)&1023);
	printf("c>>20 = 0x%08x\n", c>>20);

	printf("t4 = 0x%08x\n", t5_4[c&1023]);
	printf("t5 = 0x%08x\n", t5_5[(c>>10)&1023]);
	printf("t6 = 0x%08x\n", t5_6[c>>20]);
	printf("ret = 0x%08x\n", (a00 ^ a10 ^ a20 ^ a30 ^ t5_4[c&1023] ^ t5_5[(c>>10)&1023] ^ t5_6[c>>20]) & 1023);
	printf("\n\n");
*/

#define LowOnes ((((uint64_t)1) << 32) - 1)
#define LOW64(x) (x & LowOnes)
#define HIGH64(x) (x >> 32)

uint64_t h_hash::MultAddPrime32(uint32_t x, uint64_t a, uint64_t b)
{
	uint64_t a0, a1, c0, c1, c;
	a0 = LOW64(a) * x;
	a1 = HIGH64(a) * x;
	c0 = a0 + (a1 << 32);
	c1 = (a0>>32) + a1;
	c = (c0 & cw32_p) + (c1 >> 29) + b;
	return c;
}

int h_hash::cw32_hash(uint32_t x)
{
	uint64_t h;
	h = MultAddPrime32(x,
			MultAddPrime32(x,
				MultAddPrime32(x,
				MultAddPrime32(x, cw32_a, cw32_b),
				 cw32_c),
			cw32_d),
	   cw32_e);

	h = (h & cw32_p) + (h >> 61);
	if(h >= cw32_p) h -= cw32_p;

	// printf("a = 0x%llx\n", cw32_a);
	// printf("b = 0x%llx\n", cw32_b);
	// printf("c = 0x%llx\n", cw32_c);
	// printf("d = 0x%llx\n", cw32_d);
	// printf("e = 0x%llx\n", cw32_e);
	// printf("h = 0x%llx\n", h&1023);
	return h;
}

int h_hash::char32_hash(uint32_t x)
{
	uint8_t x0, x1, x2, x3;
	uint32_t a00, a01, a10, a11, a20, a21, a30, a31;
	uint32_t c;

	x0 = (x >> 24) & 0xff;
	x1 = (x >> 16) & 0xff;
	x2 = (x >> 8) & 0xff;
	x3 = x & 0xff;

	a00 = HIGH64(char32_t0[x0]);
	a01 = LOW64(char32_t0[x0]);

	a10 = HIGH64(char32_t1[x1]);
	a11 = LOW64(char32_t1[x1]);

	a20 = HIGH64(char32_t2[x2]);
	a21 = LOW64(char32_t2[x2]);

	a30 = HIGH64(char32_t3[x3]);
	a31 = LOW64(char32_t3[x3]);

	c = a01 + a11 + a21 + a31;
	return a00 ^ a10 ^ a20 ^ a30 ^ char32_t4[c&1023] ^ char32_t5[(c>>10)&1023] ^ char32_t6[c>>20];
}

int h_hash::short32_hash(uint32_t x)
{
	uint32_t x0, x1, x2;
	x0 = x & 65535;
	x1 = x >> 16;
	x2 = x0 + x1;
	return short32_t0[x0] ^ short32_t1[x1] ^ short32_t2[x2];
}








// cw64
uint64_t h_hash::Mod64Prime89() {
	uint64_t r0, r1, r2;
	// r2r1r0 = r&Prime89 + r>>89
	r2 = cw64_r2;
	r1 = cw64_r1;
	r0 = cw64_r0 + (r2>>25);
	r2 &= cw64_p2;
	return (r2 == cw64_p2 && r1 == cw64_p1 && r0 >= cw64_p0) ? (r0 - cw64_p0) : (r0 + (r1<<32));
}

void h_hash::MultAddPrime89(uint64_t x, uint32_t a0, uint32_t a1, uint32_t a2, uint32_t b0, uint32_t b1, uint32_t b2)
{
	uint64_t x1, x0, c21, c20, c11, c10, c01, c00;
	uint64_t d0, d1, d2, d3, s0, s1, carry;

	x1 = HIGH64(x);
	x0 = LOW64(x);
	
	c21 = a2*x1;
	c20 = a2*x0;
	
	c11 = a1*x1;
	c10 = a1*x0;
	
	c01 = a0*x1;
	c00 = a0*x0;
	
	d0 = (c20>>25)+(c11>>25)+(c10>>57)+(c01>>57);
	d1 = (c21<<7);
	d2 = (c10&cw64_p3) + (c01&cw64_p3);
	d3 = (c20&cw64_p2) + (c11&cw64_p2) + (c21>>57);
	
	s0 = b0 + LOW64(c00) + LOW64(d0) + LOW64(d1);
	cw64_r0 = LOW64(s0); carry = HIGH64(s0);
	
	s1 = b1 + HIGH64(c00) + HIGH64(d0) + HIGH64(d1) + LOW64(d2) + carry;
	cw64_r1 = LOW64(s1);
	
	carry = HIGH64(s1);
	cw64_r2 = b2 + HIGH64(d2) + d3 + carry;
}

int h_hash::cw64_hash(uint64_t x)
{
	cw64_r0 = 0;
	cw64_r1 = 0;
	cw64_r2 = 0;

	MultAddPrime89(x, cw64_a0, cw64_a1, cw64_a2, cw64_b0, cw64_b1, cw64_b2); // (r,x,A,B)
	MultAddPrime89(x, cw64_r0, cw64_r1, cw64_r2, cw64_c0, cw64_c1, cw64_c2); // (r,x,r,C)
	MultAddPrime89(x, cw64_r0, cw64_r1, cw64_r2, cw64_d0, cw64_d1, cw64_d2); // (r,x,r,D)
	MultAddPrime89(x, cw64_r0, cw64_r1, cw64_r2, cw64_e0, cw64_e1, cw64_e2); // (r,x,r,E)

	return (int)(Mod64Prime89() & 65535);
}

int h_hash::char64_hash(uint64_t x)
{
	return 1;
}

