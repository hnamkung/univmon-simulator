#ifndef __NETRO_P_H

#define __NETRO_P_H

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <list>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <time.h>
#include <sys/stat.h>


#include "library.h"
#include "countSketch.h"
#include "heavyHitters.h"
#include "detectionHelper.h"
#include "resultHelper.h"

void print_or_create_params(vector<countSketch> level_countSketch);


#endif