#include "pktFilterConfig.h"
using namespace std;

// pkt is the pointer to the packet
// return true if packet should be dropped
// return false if packet should be kept
bool PktFilterConfig::isDrop(Packet_Elem pkt) {
	// local vars
	bool match = true;
//	printf("proto: %d, flags: %x, sport: %d, dport: %d\n", ip_proto, pkt.tcp_flags, sport, dport);
    // check ip protocol
	if (this->ip_proto != -1 && this->ip_proto != pkt.ip_proto) {
        match = false;
	}
	// check tcp flags
	if (tcp_flags_set != 0 && !CHK_TCP_FALGS_SET(pkt.tcp_flags, tcp_flags_set)) {
        match = false;
	}
	if (tcp_flags_clr != 0 && CHK_TCP_FLAGS_HASSET(pkt.tcp_flags, tcp_flags_clr)) {
        match = false;
	}
	// check ports which indicates application layer protocols
	if (this->sport != -1 && this->sport != pkt.src_port) {
        match = false;
	}
	if (this->dport != -1 && this->dport != pkt.dst_port) {
        match = false;
	}
	// drop if match action is DROP and matched
	return match_action == DROP ? match : !match;
}
