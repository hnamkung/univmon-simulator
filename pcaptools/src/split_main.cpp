#include <iostream>
#include <string>

#include <time.h>
#include <unistd.h>
#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace std;

/* input argument */
char* input_file_name;
char* output_folder_name;
int pieces;
int type;

/* global variables */

pcap_t *dummy_pt;
double total_time;
double init_time;

double get_total_duration()
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *pt = pcap_open_offline(input_file_name, errbuf);

    double time;

    struct pcap_pkthdr header;
    const u_char *packet;
    while(true) {
        packet = pcap_next(pt, &header);
        if(packet == NULL)
            break;

        time = (double)header.ts.tv_sec + (double)(header.ts.tv_usec) / 1000000;
        if(init_time == 0)
            init_time = time;

    }
    pcap_close(pt);

    return (time - init_time);
}

void split_start()
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *pt = pcap_open_offline(input_file_name, errbuf);
    pcap_dumper_t * pdt = NULL;

    double checkpoint = -999999;
    double time;

    char file_name[200];

    int pcap_count = 0;
    int packet_count = 0;

    if(type == 1) {
        pieces *= 2;
    }

    struct pcap_pkthdr header;
    const u_char *packet;
    while(true) {
        packet = pcap_next(pt, &header);
        if(packet == NULL)
            break;

        time = (double)header.ts.tv_sec + (double)(header.ts.tv_usec) / 1000000;

        if(time > checkpoint + (total_time / (double)pieces)) {
            if(pdt != NULL) {
                printf("Done - %f %d \n\n", (time - checkpoint), packet_count);
                pcap_dump_close(pdt);
            }
            checkpoint = time;
            if(type == 1) {
                sprintf(file_name, "%s/%d_%d.pcap", output_folder_name, pcap_count/2, pcap_count%2);
            }
            else {
                sprintf(file_name, "%s/%d.pcap", output_folder_name, pcap_count);
            }
            pdt = pcap_dump_open(dummy_pt, file_name);
            cout << file_name << " ";
            packet_count = 0;
            pcap_count++;
        }

        pcap_dump((u_char *)pdt, &header, packet);
        packet_count++;

    }
    pcap_dump_close(pdt);
    printf("Done - %f %d \n\n", (time - checkpoint), packet_count);

    pcap_close(pt);
}

int main(int argc, char* argv[]) {

    int i;
    char errbuf[PCAP_ERRBUF_SIZE];

    if(argc != 5) {
        cout << "usage : ./split [input pcap file] [output folder] [how many pieces?] [type, 0:normal 1:change detection ...]" << endl;
        return 0;
    }

    input_file_name = argv[1];
    output_folder_name = argv[2];
    pieces = atoi(argv[3]);
    type = atoi(argv[4]);

    mkdir(output_folder_name, 0777);

    dummy_pt = pcap_open_offline("../pcaps/etc/dummy.pcap", errbuf);

    total_time = get_total_duration();

    printf("total time : %f\n", total_time);
    printf("each duration : %f\n\n", total_time/(double)pieces);

    split_start();

    pcap_close(dummy_pt);

    return 0;
}

