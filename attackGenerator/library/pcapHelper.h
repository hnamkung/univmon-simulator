#ifndef __PCAPH_H

#define __PCAPH_H

#include <pcap.h>

using namespace std;

/** Privately used, tcp flags */
enum TCP_FLAGS {
	URG,
	ACK,
	PSH,
	RST,
	SYN,
	FIN
};

/** TCP flags macro, used by create_tcp_flag_pkt to generate tcp traffic */
#define TCP_FLAG_URG (1 << URG)
#define TCP_FLAG_ACK (1 << ACK)
#define TCP_FLAG_PSH (1 << PSH)
#define TCP_FLAG_RST (1 << RST)
#define TCP_FLAG_SYN (1 << SYN)
#define TCP_FLAG_FIN (1 << FIN)

struct hun_ethhdr {
	unsigned char   h_dest[6];   /* destination eth addr */
	unsigned char   h_source[6]; /* source ether addr    */
	u_int16_t      h_proto;        /* packet type ID field */
};


struct hun_iphdr 
{ 
	#if __BYTE_ORDER == __LITTLE_ENDIAN
	unsigned int ihl:4;
	unsigned int version:4;
	#elif __BYTE_ORDER == __BIG_ENDIAN
	unsigned int version:4;
	unsigned int ihl:4;
	#else
	# error  "Please fix <bits/endian.h>"
	#endif
	u_int8_t tos;
	u_int16_t tot_len;
	u_int16_t id;
	u_int16_t frag_off;
	u_int8_t ttl;
	u_int8_t protocol;
	u_int16_t check;
	u_int32_t saddr;
	u_int32_t daddr;
	/*The options start here. */
};

struct hun_tcphdr {
	u_int16_t	source;
	u_int16_t	dest;
	u_int32_t	seq;
	u_int32_t	ack_seq;
	u_int16_t	res1:4,
		doff:4,
		fin:1,
		syn:1,
		rst:1,
		psh:1,
		ack:1,
		urg:1,
		ece:1,
		cwr:1;
	u_int16_t	window;
	u_int16_t	check;
	u_int16_t	urg_ptr;
};

struct hun_udphdr {
	u_short	uh_sport;		/* source port */
	u_short	uh_dport;		/* destination port */
	u_short	uh_ulen;		/* udp length */
	u_short	uh_sum;			/* udp checksum */
};

void pcap_get_info(char* input_file_name, double &pcap_duration, int &pcap_packet_count, uint64_t &pcap_byte_size);

double get_time_from_header(struct pcap_pkthdr *header);

void set_time_to_header(struct pcap_pkthdr *header, double time);

void pcap_merge(char* file_name1, char* file_name2, char* dst_file_name, pcap_t *dummy_pt);

// void create_syn_packet(char* packet, int packet_size, string source_ip, uint16_t source_port, string dest_ip, uint16_t dest_port);
void create_tcp_flag_pkt(char* packet, int packet_size, string source_ip, uint16_t source_port, string dest_ip, uint16_t dest_port, int flags);
void create_udp_packet(char* packet, int packet_size, string source_ip, uint16_t source_port, string dest_ip, uint16_t dest_port);
void create_tcp_packet(char* packet, int packet_size, string source_ip, uint16_t source_port, string dest_ip, uint16_t dest_port);

#endif
