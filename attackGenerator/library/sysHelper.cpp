
#include <stdlib.h>
#include <stdio.h>

#include "sysHelper.h"

void rm(char *file)
{
	char cmd[100];
	sprintf(cmd, "rm %s", file);
	system(cmd);
}

void cp(char *file1, char *file2)
{
	char cmd[100];
	sprintf(cmd, "cp %s %s", file1, file2);
	system(cmd);
}

void mv(char *file1, char *file2)
{
	char cmd[100];
	sprintf(cmd, "mv %s %s", file1, file2);
	system(cmd);
}