#ifndef __SYSH_H

#define __SYSH_H

void rm(char *file);
void cp(char *file1, char *file2);
void mv(char *file1, char *file2);

#endif