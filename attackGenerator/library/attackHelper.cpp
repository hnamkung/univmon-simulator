#include <iostream>
#include <vector>
#include <string>

#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <dirent.h>
#include <arpa/inet.h>
#include <sys/stat.h>

#include <stdio.h>
#include <stdlib.h>

#include "attackHelper.h"
#include "pcapHelper.h"
#include "sysHelper.h"


#define UDP_PACKET_SIZE 500

#define victim_prefix 11
#define bot_prefix 99

// DNS amplification attack config
#define DNS_SERVER_IP		"145.253.2.203"
#define DNS_SERVER_PORT		53
#define DNS_RESPONSE_SIZE	4096
#define DNS_QUERY_SIZE		100
// #define DNS_QUERY_DELAY		100 //milliseconds

using namespace std;

void attackHelper::reset_victim_IPs()
{
	victim_IPs.clear();
	char buf[16];
	for(int i=1; i<=victims_count; i++) {
		int level0 = victim_prefix;
		int level1 = victim_prefix;
		int level2 = (i % (256 * 256)) / 256;
		int level3 = i % 256;
		sprintf(buf, "%d.%d.%d.%d", level0, level1, level2, level3);
		string temp(buf);
		victim_IPs.push_back(temp);
	}
}

string attackHelper::getRandomVictimIP()
{
	int victim = rand()%victims_count;
	return victim_IPs[victim];
}

string attackHelper::getVictimIP(int index)
{
	return victim_IPs[index];
}

void attackHelper::reset_bot_IPs()
{
	bot_IPs.clear();
	char buf[16];
	for(int i=0; i<bots_count; i++) {
		int level0 = bot_prefix;
		int level1 = bot_prefix;
		int level2 = (i % (256 * 256)) / 256;
		int level3 = i % 256;
		// int level2 = rand()%256;
		// int level3 = rand()%256;
		sprintf(buf, "%d.%d.%d.%d", level0, level1, level2, level3);
		string temp(buf);
		bot_IPs.push_back(temp);
	}
}

string attackHelper::getRandomBotIP()
{
	int bot = rand()%bots_count;
	return bot_IPs[bot];
}

void attackHelper::createAttackPcap(char *attack_file_name, pcap_t *dummy_pt)
{
	int packet_size = 0;
	double duration = pcap_duration;
	int total_count = pcap_packet_count;
	uint64_t total_size = pcap_byte_size;
	int packet_count = 0;

	if(attackType == udpFlood) {
		// flow_dist : Mbps
		packet_size = UDP_PACKET_SIZE;
		packet_count = flow_dist * 1000 * 1000 * duration / ((double)packet_size * 8);
	} else if(attackType == synFlood) {
		// flow_dist : percentage of total traffic
		packet_size = sizeof(struct hun_ethhdr) + sizeof (struct hun_iphdr) + sizeof (struct hun_tcphdr);
		packet_count = (double)(total_count) * (double)(flow_dist/100);
	} else if(attackType == dnsAmpQueries) {
        packet_size = DNS_QUERY_SIZE;
        packet_count = (double)(total_count) * (double)(flow_dist/100);
	} else if(attackType == dnsAmpResponses) {
        packet_size = DNS_RESPONSE_SIZE;
        packet_count = (double)(total_count) * (double)(flow_dist/100);
    } else if(attackType == netroTest) { 
    	duration = 5;
        packet_size = global_packet_size;
        packet_count = global_packet_count;
    }

	pcap_dumper_t * pdt = pcap_dump_open(dummy_pt, attack_file_name);

    char packet[8192];
    char packet_resp[8192];
    bool resp_drop = true;

    for(int j=0; j<packet_count; j++) {
    	double time = (duration / (double)packet_count) * (double) j;

	    struct pcap_pkthdr header;
	    header.caplen = packet_size;
	    header.len = packet_size;
	    set_time_to_header(&header, time);

		string source_ip = getRandomBotIP();
		string dest_ip = getVictimIP(0);

		if(attackType == udpFlood) {
		    create_udp_packet(packet, packet_size, source_ip, 50000, dest_ip, 80);
		}
		else if(attackType == synFlood) {
			// generate syn packet
		    create_tcp_flag_pkt(packet, packet_size, source_ip, 50000, dest_ip, 80, TCP_FLAG_SYN);
		    // then generate some synack packet from victim, drop rate at 1/drop_rate
		    int drop_rate = 1000;
		    resp_drop = true;
		    if (j % drop_rate != 0) {
		    	resp_drop = false;
		    	create_tcp_flag_pkt(packet_resp, packet_size, dest_ip, 80, source_ip, 50000, TCP_FLAG_SYN|TCP_FLAG_ACK);
		    }
		}
		else if(attackType == dnsAmpQueries) {
			// DNS query, target port is 53
			create_udp_packet(packet, packet_size, source_ip, 5000, DNS_SERVER_IP, DNS_SERVER_PORT);
		}
		else if(attackType == dnsAmpResponses) {
			// DNS response, src port is 53.
			// Use TCP here because response size is larger than 512 bytes.
			create_tcp_packet(packet, packet_size, DNS_SERVER_IP, DNS_SERVER_PORT, dest_ip, 5000);
        }
		else if(attackType == netroTest) {
			create_udp_packet(packet, packet_size, getRandomBotIP(), 80, getRandomVictimIP(), 80);
        }

	    pcap_dump((u_char *)pdt, &header, (const u_char *)packet);
	    // dump response if reply is not dropped
	    if (!resp_drop) {
	    	pcap_dump((u_char *)pdt, &header, (const u_char *)packet_resp);
	    }
	}
	pcap_dump_close(pdt);
}

void attackHelper::injectAttacks(char* file_name, pcap_t *dummy_pt)
{
	char buf[100];
	sprintf(buf, "%s/%s", base_pcaps_folder, file_name);

	char pcap_file_name[100];
	sprintf(pcap_file_name, "%s/%s", output_pcaps_folder, file_name);

	cp(buf, pcap_file_name);
	cout << "[copy] " << pcap_file_name << endl;

	pcap_get_info(pcap_file_name, pcap_duration, pcap_packet_count, pcap_byte_size);

	char attack_file_name[100];
	sprintf(attack_file_name, "%s/attack_traffic.pcap", output_pcaps_folder);
	createAttackPcap(attack_file_name, dummy_pt);
	cout << "[create] " << attack_file_name << endl;


	char result_file_name[100];
	if(attackType == udpFlood) {
		sprintf(result_file_name, "%s/%04dMbps_total%04dMbps_%s", output_pcaps_folder, (int)flow_dist, (int)((pcap_byte_size)/(pcap_duration*1024*1024))*8, file_name);
	} else if(attackType == synFlood) {
		int pps;
		pps = (double)(pcap_packet_count) * (double)(flow_dist/100) / pcap_duration;
		sprintf(result_file_name, "%s/%.1f%%_%dpps_%s", output_pcaps_folder, flow_dist, pps, file_name);
	} else if(attackType == dnsAmpQueries) {
		sprintf(result_file_name, "%s/%.1f%%_dnsquery_%s", output_pcaps_folder, flow_dist, file_name);
    } else if(attackType == dnsAmpResponses) {
		sprintf(result_file_name, "%s/%.1f%%_dnsresp_%s", output_pcaps_folder, flow_dist, file_name);
    }

	pcap_merge(pcap_file_name, attack_file_name, result_file_name, dummy_pt);

	rm(attack_file_name);
	rm(pcap_file_name);

	cout << "[merge] " << result_file_name << endl;
	cout << "done" << endl << endl;
}

void attackHelper::start()
{
	mkdir(output_pcaps_folder, 0777);

	char empty_file_name[] = "../pcaps/etc/dummy.pcap";
	pcap_t *dummy_pt;
	char errbuf[PCAP_ERRBUF_SIZE];

	dummy_pt = pcap_open_offline(empty_file_name, errbuf);

	if(attackType == netroTest) {
		char attack_file_name[100];
		sprintf(attack_file_name, "%s/netro_test_IPs_%d_count_%d_size_%d.pcap", output_pcaps_folder, ip_count, global_packet_count, global_packet_size);
		createAttackPcap(attack_file_name, dummy_pt);
		cout << "[create] " << attack_file_name << endl;
	}
	else {
		DIR *d;
		struct dirent *dir;
		d = opendir(base_pcaps_folder);
		while ((dir = readdir(d)) != NULL)
		{
			char* file_name = dir->d_name;
			if(strstr(file_name, ".pcap") != NULL) {
				injectAttacks(file_name, dummy_pt);
				break;
			}
		}
		closedir(d);
	}

    pcap_close(dummy_pt);
}


/*
double attackHelper::generateFlowDist()
{
	int start, end;
	start = flow_dist_start * 10000;
	end = flow_dist_end * 10000;
	int r = rand()%(end+1-start) + start;

	return (double)r / 10000;
}


void attackHelper::injectAttacks_deprecated(char* file_name, pcap_t *dummy_pt)
{
	char buf[100];
	char pcap_file_name[100];
	char attack_file_name[100];
	char temp_file_name[100];

	for(int j=1; j<=get_victim_count(); j++) {
		sprintf(buf, "%s/%s", base_pcaps_folder, file_name);
		sprintf(pcap_file_name, "%s/total_%d_attacks_%s", output_pcaps_folder, j, file_name);
		sprintf(temp_file_name, "%s/temp.pcap", output_pcaps_folder);

		cp(buf, pcap_file_name);

		cout << "injecting attacks in " << pcap_file_name << endl;

		pcap_get_info(pcap_file_name, pcap_duration, pcap_packet_count, pcap_byte_size);

	    for(int i=0; i<j; i++) {
	    	reset_bot_IPs();

	    	sprintf(attack_file_name, "%s/victim%d.pcap", output_pcaps_folder, i);

	    	createAttackPcap(i, attack_file_name, dummy_pt);
	    	pcap_merge(pcap_file_name, attack_file_name, temp_file_name, dummy_pt);

	    	rm(attack_file_name);
	    	rm(pcap_file_name);
	    	mv(temp_file_name, pcap_file_name);
	    }
	}

    cout << "done" << endl << endl;
}
*/
