#ifndef __ATTACKH_H

#define __ATTACKH_H

#include <vector>
#include <stdint.h>
#include <pcap.h>

using namespace std;

class attackHelper {

public:
	enum AttackType
	{
		udpFlood,
		synFlood,
		dnsAmpQueries,
		dnsAmpResponses,
		netroTest
	};

	attackHelper()
	{

	}

	// for netro test
	attackHelper(char* _output_pcaps_folder, int _ip_count, int _packet_count, int _packet_size) {
		output_pcaps_folder = _output_pcaps_folder;
		
		ip_count = _ip_count;
		bots_count = _ip_count;
		victims_count = _ip_count;

		global_packet_count = _packet_count;
		global_packet_size = _packet_size;

		reset_victim_IPs();
		reset_bot_IPs();
	}

	attackHelper(char* _base_pcaps_folder, char* _output_pcaps_folder, int _bots_count, double _flow_dist) {
		base_pcaps_folder = _base_pcaps_folder;
		output_pcaps_folder = _output_pcaps_folder;

		bots_count = _bots_count;
		victims_count = _bots_count;

		flow_dist = _flow_dist;

		reset_victim_IPs();
		reset_bot_IPs();
	}

	AttackType attackType;

	char* base_pcaps_folder;
	char* output_pcaps_folder;
	int bots_count;
	int victims_count;
	double flow_dist;

	double pcap_duration;
	int pcap_packet_count;
	uint64_t pcap_byte_size;

	vector<string> victim_IPs;
	vector<string> bot_IPs;

	void reset_victim_IPs();
	string getRandomVictimIP();
	string getVictimIP(int index);

	void reset_bot_IPs();
	string getRandomBotIP();

	void start();
	void createAttackPcap(char *attack_file_name, pcap_t *dummy_pt);
	void injectAttacks(char* file_name, pcap_t *dummy_pt);


	// netroTest
	int ip_count;
	int global_packet_count;
	int global_packet_size;

	/*
	// deprecated
	double flow_dist_start;
	double flow_dist_end;

	double generateFlowDist();
	int get_victim_count();
	*/


};

#endif
