#include <iostream>
#include <string>

#include <time.h>
#include <unistd.h>
#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/socket.h>


#include "pcapHelper.h"

using namespace std;


void pcap_get_info(char* input_file_name, double &pcap_duration, int &pcap_packet_count, uint64_t &pcap_byte_size)
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *pt = pcap_open_offline(input_file_name, errbuf);

    double time;
    double init_time = 0;

    int count = 0;
    uint64_t size = 0;

    struct pcap_pkthdr header;
    const u_char *packet;
    while(true) {
        packet = pcap_next(pt, &header);
        if(packet == NULL)
            break;

        time = get_time_from_header(&header);
        if(init_time == 0)
            init_time = time;

        count ++;

        struct hun_ethhdr *ethh = (struct hun_ethhdr *) packet;
        if(ntohs(ethh->h_proto) == ETHERTYPE_IP) {
            struct hun_iphdr *iph = (struct hun_iphdr *) (packet + sizeof(struct hun_ethhdr));
            size += ntohs(iph->tot_len) + 18;
        }
    }
    pcap_close(pt);

    pcap_duration = time - init_time;
    pcap_packet_count = count;
    pcap_byte_size = size;
}

double get_time_from_header(struct pcap_pkthdr *header)
{
    return (double)header->ts.tv_sec + (double)(header->ts.tv_usec) / 1000000;
}

void set_time_to_header(struct pcap_pkthdr *header, double time)
{
    double sec = (int) time;
    header->ts.tv_sec = sec;
    header->ts.tv_usec = (time - sec) * 1000 * 1000;
}

void pcap_merge(char* file_name1, char* file_name2, char* dst_file_name, pcap_t *dummy_pt)
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *pt1 = pcap_open_offline(file_name1, errbuf);
    pcap_t *pt2 = pcap_open_offline(file_name2, errbuf);
    pcap_dumper_t * pdt = pcap_dump_open(dummy_pt, dst_file_name);

    double init_time1, init_time2;
    double time1, time2;

    struct pcap_pkthdr header1, header2;
    const u_char *packet1, *packet2;

    packet1 = pcap_next(pt1, &header1);
    init_time1 = get_time_from_header(&header1);
    time1 = 0;

    packet2 = pcap_next(pt2, &header2);
    init_time2 = get_time_from_header(&header2);
    set_time_to_header(&header2, init_time1);
    time2 = 0;

    while(true) {
        if(packet1 == NULL && packet2 == NULL)
            break;

        if(time1 < time2) {
            pcap_dump((u_char *)pdt, &header1, (const u_char *)packet1);
            packet1 = pcap_next(pt1, &header1);
            if(packet1 == NULL)
                time1 = 999999;
            else
                time1 = get_time_from_header(&header1) - init_time1;
        }
        else {
            pcap_dump((u_char *)pdt, &header2, (const u_char *)packet2);
            packet2 = pcap_next(pt2, &header2);
            if(packet2 == NULL) {
                time2 = 999999;
            }
            else {
                time2 = get_time_from_header(&header2) - init_time2;
                set_time_to_header(&header2, init_time1 + time2);
            }
        }
    }

    pcap_close(pt1);
    pcap_close(pt2);
    pcap_dump_close(pdt);
}

void create_tcp_flag_pkt(char* packet, int packet_size, string source_ip, uint16_t source_port, string dest_ip, uint16_t dest_port, int flags)
{
    memset (packet, 0, packet_size); /* zero out the buffer */
    char ip[32];

    // Ethernet header
    struct hun_ethhdr *ethh = (struct hun_ethhdr *) packet;
    ethh->h_proto = htons(ETHERTYPE_IP); // IPv4 : 0x0800

    //IP header
    struct hun_iphdr *iph = (struct hun_iphdr *) (packet + sizeof(struct hun_ethhdr));

    //TCP header
    struct hun_tcphdr *tcph = (struct hun_tcphdr *) (packet + sizeof(struct hun_ethhdr) + sizeof (struct ip));

    struct sockaddr_in sin;

    iph->ihl = 5;
    iph->version = 4;
    iph->tos = 0;
    iph->tot_len = htons(u_int16_t(packet_size - sizeof(struct hun_ethhdr)));
    iph->id = htons(54321);  //Id of this packet
    iph->frag_off = 0;
    iph->ttl = 255;
    iph->protocol = IPPROTO_TCP;
    iph->check = 0;      //Set to 0 before calculating checksum
    iph->saddr = inet_addr ( source_ip.c_str() );
    iph->daddr = inet_addr ( dest_ip.c_str() );
    // iph->check = csum ((unsigned short *) packet, iph->tot_len >> 1);

    // //TCP Header
    tcph->source = htons (source_port);
    tcph->dest = htons (dest_port);
    tcph->seq = 0;
    tcph->ack_seq = 0;
    tcph->doff = 5;      /* first and only tcp segment */

    tcph->fin = !!(flags & TCP_FLAG_FIN);
    tcph->syn = !!(flags & TCP_FLAG_SYN);
    tcph->rst = !!(flags & TCP_FLAG_RST);
    tcph->psh = !!(flags & TCP_FLAG_PSH);
    tcph->ack = !!(flags & TCP_FLAG_ACK);
    tcph->urg = !!(flags & TCP_FLAG_URG);
    tcph->window = htons (5840); /* maximum allowed window size */
    tcph->check = 0;
    tcph->urg_ptr = 0;
    // tcph->check = csum( (unsigned short*) &psh , sizeof (struct pseudo_header));
}

void create_udp_packet(char* packet, int packet_size, string source_ip, uint16_t source_port, string dest_ip, uint16_t dest_port)
{
    memset (packet, 0, packet_size); /* zero out the buffer */
    char ip[32];

    // Ethernet header
    struct hun_ethhdr *ethh = (struct hun_ethhdr *) packet;
    ethh->h_proto = htons(ETHERTYPE_IP); // IPv4 : 0x0800

    //IP header
    struct hun_iphdr *iph = (struct hun_iphdr *) (packet + sizeof(struct hun_ethhdr));

    //TCP header
    struct hun_udphdr *udph = (struct hun_udphdr *) (packet + sizeof(struct hun_ethhdr) + sizeof (struct hun_iphdr));

    struct sockaddr_in sin;

    iph->ihl = 5;
    iph->version = 4;
    iph->tos = 0;
    iph->tot_len = htons(u_int16_t(packet_size - sizeof(struct hun_ethhdr)));
    iph->id = htons(54321);  //Id of this packet
    iph->frag_off = 0;
    iph->ttl = 255;
    iph->protocol = IPPROTO_UDP;
    iph->check = 0;      //Set to 0 before calculating checksum
    iph->saddr = inet_addr ( source_ip.c_str() );
    iph->daddr = inet_addr ( dest_ip.c_str() );
    // iph->check = csum ((unsigned short *) packet, iph->tot_len >> 1);

    // //TCP Header
    udph->uh_sport = htons (source_port);
    udph->uh_dport = htons (dest_port);
    udph->uh_ulen = htons(u_int16_t(packet_size - sizeof(struct hun_ethhdr) - sizeof(struct hun_iphdr)));

    // tcph->check = csum( (unsigned short*) &psh , sizeof (struct pseudo_header));

}

void create_tcp_packet(char* packet, int packet_size, string source_ip, uint16_t source_port, string dest_ip, uint16_t dest_port)
{
    memset (packet, 0, packet_size); /* zero out the buffer */
	char ip[32];

	// Ethernet header
    struct hun_ethhdr *ethh = (struct hun_ethhdr *) packet;
    ethh->h_proto = htons(ETHERTYPE_IP); // IPv4 : 0x0800

    //IP header
    struct hun_iphdr *iph = (struct hun_iphdr *) (packet + sizeof(struct hun_ethhdr));

    //TCP header
    struct hun_tcphdr *tcph = (struct hun_tcphdr *) (packet + sizeof(struct hun_ethhdr) + sizeof (struct ip));

    struct sockaddr_in sin;

    iph->ihl = 5;
    iph->version = 4;
    iph->tos = 0;
    iph->tot_len = htons(u_int16_t(packet_size - sizeof(struct hun_ethhdr)));
    iph->id = htons(54321);  //Id of this packet
    iph->frag_off = 0;
    iph->ttl = 255;
    iph->protocol = IPPROTO_TCP;
    iph->check = 0;      //Set to 0 before calculating checksum
    iph->saddr = inet_addr ( source_ip.c_str() );
    iph->daddr = inet_addr ( dest_ip.c_str() );
    // iph->check = csum ((unsigned short *) packet, iph->tot_len >> 1);

    // //TCP Header
    tcph->source = htons (source_port);
    tcph->dest = htons (dest_port);
    tcph->seq = 0;
    tcph->ack_seq = 0;
    tcph->doff = 5;      /* first and only tcp segment */
    tcph->fin=0;
    tcph->syn=0;
    tcph->rst=0;
    tcph->psh=0;
    tcph->ack=0;
    tcph->urg=0;
    tcph->window = htons (5840); /* maximum allowed window size */
    tcph->check = 0;
    tcph->urg_ptr = 0;
    // tcph->check = csum( (unsigned short*) &psh , sizeof (struct pseudo_header));
}


