#include <iostream>

#include <stdio.h>
#include <stdlib.h>

#include "udpFlood.h"
#include "../library/attackHelper.h"

using namespace std;

attackHelper udpAttackHelper;

void udp_flood_main(int argc, char* argv[])
{
	if(argc != 6) {
		printf("usage : ./attackGenerator udpFlood [base pcaps folder] [output pcaps folder] [# of bots] [flow distribution(Mbps)]\n");
		return;
	}

	char* base_pcaps_folder = argv[2];
	char* output_pcaps_folder = argv[3];
	int bots_count = atoi(argv[4]);
	double flow_dist = atof(argv[5]);

	udpAttackHelper = attackHelper(base_pcaps_folder, output_pcaps_folder, bots_count, flow_dist);
	udpAttackHelper.attackType = attackHelper::udpFlood;
	udpAttackHelper.start();
}





