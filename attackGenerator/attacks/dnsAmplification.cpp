#include <iostream>

#include <stdio.h>
#include <stdlib.h>

#include "dnsAmplification.h"
#include "../library/attackHelper.h"

using namespace std;

attackHelper dnsQueriesHelper;
attackHelper dnsResponsesHelper;

void dns_amplification_main(int argc, char* argv[])
{
  if(argc != 7) {
    printf("usage : ./attackGenerator dnsAmplification [base pcaps folder] "\
           "[output queries pcaps folder] [output responses pcaps folder] "\
           "[# of bots] [flow distribution(%% of total flow)] \n");
    return;
  }

  char* base_pcaps_folder = argv[2];
  char* output_queries_folder = argv[3];
  char* output_responses_folder = argv[4];
  int bots_count = atoi(argv[5]); // 1-10 bots
  double flow_dist = atof(argv[6]);

  // Need to generate DNS query and reponse pcap files separately
  dnsQueriesHelper = attackHelper(base_pcaps_folder, output_queries_folder, bots_count, flow_dist);
  dnsQueriesHelper.attackType = attackHelper::dnsAmpQueries;
  dnsQueriesHelper.start();

  dnsResponsesHelper = attackHelper(base_pcaps_folder, output_responses_folder, bots_count, flow_dist);
  dnsResponsesHelper.attackType = attackHelper::dnsAmpResponses;
  dnsResponsesHelper.start();
}





