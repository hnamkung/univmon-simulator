#include <iostream>

#include <stdio.h>
#include <stdlib.h>

#include "netroTest.h"
#include "../library/attackHelper.h"

using namespace std;

attackHelper netroAttackHelper;

void netro_test_main(int argc, char* argv[])
{
	if(argc != 6) {
		printf("usage : ./attackGenerator netroTest [output pcaps folder] [# of src/dst IPs] [packet count] [packet size]\n");
		return;
	}

	char* output_pcaps_folder = argv[2];
	int ip_count = atoi(argv[3]);
	int packet_count = atoi(argv[4]);
	int packet_size = atoi(argv[5]);

	netroAttackHelper = attackHelper(output_pcaps_folder, ip_count, packet_count, packet_size);
	netroAttackHelper.attackType = attackHelper::netroTest;
	netroAttackHelper.start();
}