#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#include "attacks/synFlood.h"
#include "attacks/udpFlood.h"
#include "attacks/dnsAmplification.h"
#include "attacks/netroTest.h"

int main(int argc, char* argv[])
{
	srand (time(NULL));

	if(argc < 2) {
		printf("usage : ./attackGenerator [attack_name]\n");
	}
	else {
		if(strcmp(argv[1], "synFlood") == 0) {
			syn_flood_main(argc, argv);
		}
		else if(strcmp(argv[1], "udpFlood") == 0) {
			udp_flood_main(argc, argv);
		}
		else if(strcmp(argv[1], "dnsAmplification") == 0) {
			dns_amplification_main(argc, argv);
		}
		else if(strcmp(argv[1], "netroTest") == 0) {
			netro_test_main(argc, argv);
		}
	}
	return 0;
}
